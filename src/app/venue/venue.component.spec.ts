import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { PageTitleComponent } from '@core/components/page-title/page-title.component';
import { MenuComponent } from '@core/menu/menu.component';
import { MockComponents } from 'ng-mocks';
import { VenueComponent } from './venue.component';

describe('VenueComponent', () => {
    let component: VenueComponent;
    let fixture: ComponentFixture<VenueComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            declarations: [VenueComponent, MockComponents(MenuComponent, PageTitleComponent)],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(VenueComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});

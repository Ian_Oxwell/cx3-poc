import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';
import { RedirectComponent } from './redirect.component';

@NgModule({
    imports: [CommonModule, RouterModule, MatButtonModule],
    declarations: [RedirectComponent],
})
export class RedirectModule {}

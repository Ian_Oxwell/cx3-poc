import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RedirectComponent } from './redirect.component';


describe('RedirectComponent', () => {
	let component: RedirectComponent;
	let fixture: ComponentFixture<RedirectComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [],
			declarations: [ RedirectComponent ],
		providers: []
		})
		.compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(RedirectComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});

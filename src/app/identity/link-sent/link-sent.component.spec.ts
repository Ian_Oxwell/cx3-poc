import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LinkSentComponent } from './link-sent.component';


describe('LinkSentComponent', () => {
	let component: LinkSentComponent;
	let fixture: ComponentFixture<LinkSentComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [],
			declarations: [ LinkSentComponent ],
		providers: []
		})
		.compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(LinkSentComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});

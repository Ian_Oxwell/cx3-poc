import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ComponentBase } from '@core/base/base.component.base';
import { HeroImageService } from '@core/hero-image/service/hero-image.service';
import { timer } from 'rxjs';
import { take, takeUntil, tap } from 'rxjs/operators';
import { IFormModel } from '../models/identity-forms.model';

@Component({
    template: '',
})
// eslint-disable-next-line @angular-eslint/component-class-suffix
export abstract class IdentityBase extends ComponentBase {
    form: FormGroup = {} as FormGroup;
    abstract formModel: IFormModel;
    slug: string = '';

    constructor(private fb: FormBuilder, private heroImageService: HeroImageService, private router: Router) {
        super();
    }

    ngOnInit(): void {
        this.form = this.createForm(this.formModel);
    }

    /**
     * Short method to not inject form builder in each component.
     * @param formModel Form model to build, including validation.
     * @returns completed Form.
     */
    createForm(formModel: IFormModel): FormGroup {
        return this.fb.group(formModel);
    }

    /**
     * Listens for the first slug result only.
     */
    getCurrentSlug(): void {
        this.heroImageService
            .getCurrentSlug()
            .pipe(
                take(1),
                tap((slug: string) => (this.slug = slug))
            )
            .subscribe();
    }

    /**
     * Resets form by clearing all fields and setting all fields and form as pristine.
     */
    resetForm(): void {
        this.form.reset();
    }

    /**
     * Small method to engage navigation to url with a delay.
     * @param url Url to navigate to - no need to include slug `/example'
     * @param delayMilliseconds soft delay before navigating.
     */
    navigateToUrl(url: string, delayMilliseconds: number): void {
        timer(delayMilliseconds)
            .pipe(
                tap((_) => this.router.navigate([`/${this.slug}${url}`])),
                takeUntil(this.ngUnsubscribe)
            )
            .subscribe();
    }
}

import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Message, MessageStatus } from '@core/components/toast/model/message.model';
import { MessageService } from '@core/components/toast/service/message.service';
import { HeroImageService } from '@core/hero-image/service/hero-image.service';
import { MessageResult } from '@models/common.model';
import { of } from 'rxjs';
import { catchError, filter, switchMap, takeUntil, tap } from 'rxjs/operators';
import { IdentityBase } from '../base/identity.component.base';
import { IUser } from '../models/user.model';
import { IdentityFormService } from '../service/identity-form.service';
import { LoginService } from '../service/login.service';

@Component({
    selector: 'app-details',
    templateUrl: './details.component.html',
    styleUrls: ['./details.component.scss'],
})
export class DetailsComponent extends IdentityBase implements OnInit {
    formModel = {
        email: [
            this.identityFormService.tempUserEmail,
            [Validators.required, Validators.minLength(2), Validators.maxLength(120), Validators.email],
        ],
        givenNames: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(120)]],
        familyName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(120)]],
        mobileNumber: ['', [Validators.minLength(8), Validators.maxLength(14)]],
    };

    loginSuccess: Message = {
        severity: MessageStatus.Success,
        summary: 'Successful login',
        life: 12000,
    };

    alreadyRegisteredMessage: Message = {
        severity: MessageStatus.Alert,
        summary: 'Email address is already registered',
        life: 12000,
    };

    loginError: Message = {
        severity: MessageStatus.Error,
        summary: 'Login Error in form',
        detail: 'TODO - take http error details here',
        life: 12000,
    };

    formErrorsMessage = (errorList: string[]): Message => {
        return {
            severity: MessageStatus.Error,
            summary: 'Form Errors, please correct:',
            detail: errorList.join(`, `),
            life: 12000,
        };
    };

    constructor(
        private identityFormService: IdentityFormService,
        private messageService: MessageService,
        private loginService: LoginService,
        router: Router,
        heroImageService: HeroImageService,
        fb: FormBuilder
    ) {
        super(fb, heroImageService, router);
    }

    /**
     * Submit Form action - show errors.
     */
    submitForm(): void {
        this.form.markAllAsTouched();
        this.form.updateValueAndValidity();

        if (this.form.valid) {
            // TODO logic for registration
            const rawForm: IUser = this.form.getRawValue() as IUser;
            this.registerAccount(rawForm);
        } else {
            const errors = this.identityFormService.getListErrors(this.form);
            this.messageService.add(this.formErrorsMessage(errors));
        }
    }

    registerAccount(user: IUser): void {
        this.loginService
            .registerAccount(user)
            .pipe(
                tap((result: MessageResult) => {
                    if (result.message === 'Email address is already registered') {
                        this.messageService.add(this.alreadyRegisteredMessage);
                    }
                }),
                filter((result: MessageResult) => typeof Number(result.message) === 'number'),
                switchMap((_) => this.loginService.login(user.email)),
                tap((message: MessageResult) => {
                    if (message.message === 'Successful login') {
                        this.messageService.add(this.loginSuccess);
                        this.navigateToUrl('', 1000);
                    }
                }),
                catchError((err: HttpErrorResponse) => {
                    this.messageService.add(this.loginError);
                    return of();
                }),
                takeUntil(this.ngUnsubscribe)
            )
            .subscribe();
    }
}

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { ErrorkeysPipe } from '@core/form/error-pipe/error-keys.pipe';
import { FormErrorMessagePipe } from '@core/form/error-pipe/form-error-message.pipe';
import { HeroImageService } from '@core/hero-image/service/hero-image.service';
import { autoSpy, Spy } from '@tests/auto-spy';
import { MockPipes } from 'ng-mocks';
import { of } from 'rxjs';
import { IdentityFormService } from '../service/identity-form.service';
import { DetailsComponent } from './details.component';

describe('DetailsComponent', () => {
    let component: DetailsComponent;
    let fixture: ComponentFixture<DetailsComponent>;

    const heroImageServiceSpy: Spy<HeroImageService> = autoSpy(HeroImageService);
    heroImageServiceSpy.getCurrentSlug.and.returnValue(of('test'));

    const identityFormServiceSpy: Spy<IdentityFormService> = autoSpy(IdentityFormService);

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                HttpClientTestingModule,
                ReactiveFormsModule,
                FormsModule,
                MatIconModule,
                MatButtonModule,
                MatCardModule,
                MatDividerModule,
                MatFormFieldModule,
                MatInputModule,
                NoopAnimationsModule,
            ],
            declarations: [DetailsComponent, MockPipes(ErrorkeysPipe, FormErrorMessagePipe)],
            providers: [
                { provide: HeroImageService, useValue: heroImageServiceSpy },
                { provide: IdentityFormService, useValue: identityFormServiceSpy },
            ],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(DetailsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});

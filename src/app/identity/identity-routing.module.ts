import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailsComponent } from './details/details.component';
import { EmailAddressComponent } from './email-address/email-address.component';
import { IdentityComponent } from './identity.component';
import { LinkSentComponent } from './link-sent/link-sent.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';

const routes: Routes = [
    {
        path: '',
        component: IdentityComponent,
        children: [
            {
                path: 'email',
                component: EmailAddressComponent,
                data: { title: 'Hey There' },
            },
            {
                path: 'link-sent',
                component: LinkSentComponent,
                data: { title: 'Link Sent' },
            },
            {
                path: 'verify',
                component: VerifyEmailComponent,
                data: { title: 'Verify Link' },
            },
            {
                path: 'details',
                component: DetailsComponent,
                data: { title: 'Hey There' },
            },
        ],
    },
    { path: '', redirectTo: 'email' },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class IdentityRoutingModule {}

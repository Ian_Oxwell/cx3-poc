
export interface IPwaSubscriptionKey {
	id: number;
	deviceName: string;
}

export interface IUser {
	email: string;
	familyName: string;
	givenNames: string;
	displayName?: string;
	mobileNumber: string;
	subscriptionKeys?: IPwaSubscriptionKey[];
}
import { IDictionary } from "src/app/core/models/common.model";


/** Token interface */
export interface IToken {
	token: string;
	lifetime: number; // The lifetime duration of the token.
	expiresAt: number; // The absolute time that the token will expire (measured using the client local clock).
	refreshToken: string;
}

/** Token Response Interface */
export interface IResponseToken {
	token: string;
	expiresIn: number;
	refreshToken: string;
}

export interface ITokenState {
	expiresAt: number;
	refreshesAt: number;
	warnsAt: number;
}

// TODO verify if needed
export interface IClaims {
	exp: any;
	sub: number; // UserId - RFC 7519 compliant https://tools.ietf.org/html/rfc7519#section-4.1
	name: string;
	givenname: string;
	surname: string;
	roles: string[];
	permissions: IDictionary<number[]>;
}
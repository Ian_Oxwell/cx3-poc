import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Message, MessageStatus } from '@core/components/toast/model/message.model';
import { MessageService } from '@core/components/toast/service/message.service';
import { of } from 'rxjs';
import { catchError, takeUntil, tap } from 'rxjs/operators';
import { HeroImageService } from 'src/app/core/hero-image/service/hero-image.service';
import { MessageResult } from 'src/app/core/models/common.model';
import { IdentityBase } from '../base/identity.component.base';
import { IdentityFormService } from '../service/identity-form.service';
import { LoginService } from '../service/login.service';

@Component({
    selector: 'app-email-address',
    templateUrl: './email-address.component.html',
    styleUrls: ['./email-address.component.scss'],
})
export class EmailAddressComponent extends IdentityBase {
    formModel = {
        email: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(120), Validators.email]],
    };

    isNewRegistration = false;

    loginSuccess: Message = {
        severity: MessageStatus.Success,
        summary: 'Successful login',
        life: 12000,
    };

    incompleteForm: Message = {
        severity: MessageStatus.Information,
        summary: 'Something is missing from form',
        detail: 'TODO - list of form errors',
        life: 12000,
    };

    accountNotFound: Message = {
        severity: MessageStatus.Information,
        summary: 'Account not found',
        detail: 'Would you like to register?',
        life: 12000,
    };

    loginError: Message = {
        severity: MessageStatus.Error,
        summary: 'Login Error in form',
        detail: 'TODO - take http error details here',
        life: 12000,
    };

    constructor(
        private formService: IdentityFormService,
        private loginService: LoginService,
        private messageService: MessageService,
        heroImageService: HeroImageService,
        router: Router,
		fb: FormBuilder
    ) {
        super(fb, heroImageService, router);
    }

    /**
     * Triggered login process from the template, displays message if form is not valid.
     */
    login(): void {
        if (!this.form.valid) {
            this.messageService.add(this.incompleteForm);
            return;
        }
        const formRaw = this.form.getRawValue();
        this.loginAccount(formRaw.email);
    }

    /**
     * Act on the account login process
     * @param email Email address to attempt login / obtain token.
     */
    loginAccount(email: string): void {
        this.loginService.login(email)
            .pipe(
                tap((message: MessageResult) => {
                    if (message.message === 'Successful login') {
                        this.messageService.add(this.loginSuccess);
                        this.navigateToUrl('', 1000);
                    } else if (message.message === 'Register new account') {
                        this.messageService.add(this.accountNotFound);
                        this.registerAccount();
                    }
                    return message;
                }),
                catchError((err: HttpErrorResponse) => {
                    this.messageService.add(this.loginError);
                    return of();
                }),
                takeUntil(this.ngUnsubscribe)
            )
            .subscribe();
    }

    /**
     * Salvage what we can from the form already and navigate to the register / details component.
     */
    registerAccount(): void {
        this.formService.tempUserEmail = this.form.get('email')?.value;
        this.navigateToUrl(`/account/details`, 100);
    }
}

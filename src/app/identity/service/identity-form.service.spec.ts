import { TestBed } from '@angular/core/testing';
import { IdentityFormService } from './identity-form.service';


describe('CreateFormService', () => {
  let service: IdentityFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({
		providers: [IdentityFormService],
	});
    service = TestBed.inject(IdentityFormService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

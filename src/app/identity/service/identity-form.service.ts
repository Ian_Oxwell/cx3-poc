import { Injectable } from '@angular/core';
import { FormGroup, ValidationErrors } from '@angular/forms';

@Injectable()
export class IdentityFormService {
    tempUserEmail: string = '';

    /**
     * If a control has an error it adds the control name to the string array.
     * @returns list array of controls with errors in them.
     */
    getListErrors(form: FormGroup): string[] {
        const errorList: string[] = [];
        Object.keys(form.controls).forEach((controlKey: string) => {
            const controlErrors: ValidationErrors | undefined | null = form.get(controlKey)?.errors;
            if (!!controlErrors) {
                errorList.push(controlKey);
            }
        });

        return errorList;
    }
}

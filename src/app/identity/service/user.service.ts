import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { IUser } from '../models/user.model';

@Injectable({
    providedIn: 'root',
})
export class UserService {
	private isLoggedInSubject$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
	isLoggedIn: Observable<boolean> = this.isLoggedInSubject$.asObservable();

	private userProfileSubject$: BehaviorSubject<IUser | null> = new BehaviorSubject<IUser | null>(null);
	userProfile: Observable<IUser | null> = this.userProfileSubject$.asObservable();

	constructor() {}

    setLoggedIn(isLoggedIn: boolean) {
        this.isLoggedInSubject$.next(isLoggedIn);
    }

	setData(userProfile: IUser | null) {
		this.userProfileSubject$.next(userProfile);
	}
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
import { MessageResult } from 'src/app/core/models/common.model';
import { StorageService } from 'src/app/core/services/storage.service';
import { environment } from 'src/environments/environment';
import { IClaims, IResponseToken, IToken, ITokenState } from '../models/token.model';
import { IUser } from '../models/user.model';
import { UserService } from './user.service';

@Injectable({
    providedIn: 'root',
})
export class LoginService {
    private authentication$: BehaviorSubject<ITokenState | null> = new BehaviorSubject<ITokenState | null>(null);

    // JWT token string obtained at login.
    private jwt: IToken = this.initNewJwt();
    // Key used for storing the token in local storage.
    // Must match token name used in auth.module.ts
    private tokenKey = 'token';
    private refreshKey = 'refreshToken';
    private expiryKey = 'App.Logout.ExpiryMillisec';

    constructor(private http: HttpClient, private storageService: StorageService, private userService: UserService) {
        this.getSetJwtInitial();
    }

    /** In case any components need access to the Token State */
    public getAuthentication(): Observable<ITokenState | null> {
        return this.authentication$.asObservable();
    }

    /**
     * Login Method - Gets both an access token and a refresh token
     * @param email the provided username
     */
    public login(email: string): Observable<MessageResult> {
        // Clear and current session
        this.hardLogout();

        // Construct the http request
        const body = { email };
        const loginUrl = `${environment.apiUrl}/token/create?email=${email}`;

        return this.http.post<IResponseToken | MessageResult>(loginUrl, body).pipe(
            switchMap((response: IResponseToken | MessageResult) => {
                if (response.hasOwnProperty('message')) {
                    return of(response as MessageResult);
                } else {
                    const jwt = this.processJwtResponse(response as IResponseToken);
                    this.userService.setLoggedIn(true);
                    const success = this.setJwt(jwt);
                    return of(new MessageResult(success ? 'Successful login' : 'Login failure'));
                }
            })
        );
    }

    /** Clears login state. */
    public hardLogout(): void {
        this.jwt = this.initNewJwt();
        this.storageService.removeItem(this.tokenKey);
        this.userService.setLoggedIn(false);
        this.userService.setData(null);
        this.authentication$.next(null);
    }

    /**
     * Registers a new account with DB.
     * @param user Trimmed down user profile.
     * @returns A message result - like successful.
     */
    public registerAccount(user: IUser): Observable<MessageResult> {
        const loginUrl = `${environment.apiUrl}/user/user`;
        return this.http.post<MessageResult>(loginUrl, user);
    }

    /**
     * Gets the currently logged in users profile (included subscribed devices).
     * @returns Observable of the User profile or null.
     */
    public getSingleUserProfile(): Observable<IUser | null> {
        const jwtToken = this.getJwt();
        if (jwtToken === null) {
            return of(null);
        }
        const userId = this.decode(jwtToken)?.sub;
        return !userId
            ? of(null)
            : this.http.get<IUser>(`${environment.apiUrl}/user/user?id=${userId}`).pipe(
                  tap((user: IUser) => {
                      console.log('logged in user', user);
                      this.userService.setData(user);
                      this.userService.setLoggedIn(true);
                  })
              );
    }

	/**
	 * FAKE a user login - for testing purposes ONLY.
	 * TODO: Remove this.
	 * @returns true if jwt set correctly.
	 */
	public fakeJwt(): boolean {
        const fakeJwt: IResponseToken = {
            token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxIiwiZW1haWwiOiJib2JAYm9iLmNvbSIsImdpdmVubmFtZSI6Ik9uZSIsImZhbWlseW5hbWUiOiJUZXN0IiwiZXhwIjoxNjMwMDQ2NDQwLCJuYmYiOjE2Mjk5NjAwNDAsImlhdCI6MTYyOTk2MDA0MCwiaXNzIjoiQ1gzIiwiYXVkIjoiaHR0cDovL2xvY2FsaG9zdDo0NDM5MyJ9.-KeA1UF5ko8RRjWvteCecw6EO26FzpgybMBN8gPrLeg',
            expiresIn: 1440,
            refreshToken: '93131c78d5fb4366960d8f1f725a5b00',
        };
		const fakeUser: IUser = {
			email: 'fake.user@fake.com',
			familyName: 'Joker',
			givenNames: 'Fake',
			mobileNumber: 'Not a real mobile'
		};
        const jwt = this.processJwtResponse(fakeJwt);
		this.userService.setData(fakeUser);
		this.userService.setLoggedIn(true);
        return this.setJwt(jwt);
    }

    /** Decodes the JWT Token */
    private decode(token: string): IClaims | null {
        if (token === null || token.length < 8) {
            return null;
        }

        // Decode encrypted token
        return this.parseJwt(token);
    }

    /** Jwt parser taken from https://stackoverflow.com/questions/38552003/how-to-decode-jwt-token-in-javascript-without-using-a-library */
    private parseJwt(token: string): IClaims | null {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        var jsonPayload = decodeURIComponent(
            atob(base64)
                .split('')
                .map(function (c) {
                    return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
                })
                .join('')
        );
        try {
            return JSON.parse(jsonPayload);
        } catch {
            return null;
        }
    }

    /**
     * Initialises a new Jwt Object
     */
    private initNewJwt(): IToken {
        return {
            token: '',
            refreshToken: '',
            expiresAt: 0,
            lifetime: 0,
        };
    }

    /** 'Installs' a new JWT */
    private setJwt(newJwt: IToken): boolean {
        // Default
        if (!newJwt.token || !newJwt.refreshToken || !newJwt.expiresAt) {
            this.jwt = this.initNewJwt();

            this.storeJwt(this.jwt);

            // Trigger Activity Change.
            this.authentication$.next(null);

            return false;
        }

        // Set token properties
        this.jwt = newJwt;

        this.storeJwt(this.jwt);

        const expiresAt = newJwt.expiresAt;
        const lifetime = newJwt.lifetime;

        const refreshesAt = expiresAt - Math.floor(lifetime / 2);
        const warnsAt = expiresAt - Math.floor(lifetime / 4);

        this.authentication$.next({ expiresAt, refreshesAt, warnsAt });

        // TODO: Google Analytics - Requires UserId
        // (window as any).ga('set', 'userId', this.claims.sub);

        return true;
    }

    /**
     * The common method for processing the new JWT (login and refresh)
     */
    private processJwtResponse(response: IResponseToken): IToken {
        let newJwt = this.initNewJwt();

        // Successful if there is a JWT Response
        if (!!response) {
            const expiresIn = response.expiresIn || 0;
            const lifetime = expiresIn * 1000; // Convert to milliseconds.
            const expiresAt = Date.now() + lifetime;
            newJwt = {
                token: !!response.token ? response.token : '',
                refreshToken: !!response.refreshToken ? response.refreshToken : '',
                lifetime,
                expiresAt,
            };
        }

        return newJwt;
    }

    /** On service construction check local storage and set jwt appropriately. */
    private getSetJwtInitial(): void {
        this.restoreJwt();

        // Observe the session for expiry changes from other tabs.
        this.storageService
            .observeItem(this.expiryKey)
            .pipe(
                distinctUntilChanged(),
                tap(() => this.restoreJwt())
            )
            .subscribe();
    }

    /** Restores the JWT from session storage. */
    private restoreJwt(): void {
        this.setJwt({
            token: this.getJwt(),
            refreshToken: this.getJwtRefresh(),
            expiresAt: this.getJwtExpiry(),
            lifetime: this.getJwtExpiry() - Date.now(),
        });
    }
    // #region Storage Setters/Getters
    /** Saves the JWT token details in storage */
    private storeJwt(jwt: IToken): void {
        this.storageService.setItem(this.tokenKey, jwt.token);
        this.storageService.setItem(this.refreshKey, jwt.refreshToken);
        this.storageService.setItem(this.expiryKey, jwt.expiresAt.toString());
    }

    /** Gets the JWT Token from storage */
    public getJwt(): string {
        return this.storageService.getItem(this.tokenKey) as string;
    }

    /** Gets the JWT refresh token from storage */
    private getJwtRefresh(): string {
        return this.storageService.getItem(this.refreshKey) as string;
    }

    /** Gets the JWT expiry time from storage */
    private getJwtExpiry(): number {
        return parseInt(this.storageService.getItem(this.expiryKey) as string, 10);
    }
}

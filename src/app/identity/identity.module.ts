import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { RouterModule } from '@angular/router';
import { PageTitleModule } from '@core/components/page-title/page-title.module';
import { ErrorPipeModule } from '@core/form/error-pipe/error-pipe.module';
import { DetailsComponent } from './details/details.component';
import { EmailAddressComponent } from './email-address/email-address.component';
import { IdentityRoutingModule } from './identity-routing.module';
import { IdentityComponent } from './identity.component';
import { LinkSentComponent } from './link-sent/link-sent.component';
import { IdentityFormService } from './service/identity-form.service';
import { VerifyEmailComponent } from './verify-email/verify-email.component';


@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        MatIconModule,
        MatButtonModule,
        MatCardModule,
		MatDividerModule,
		MatFormFieldModule,
		MatInputModule,
        IdentityRoutingModule,
        RouterModule,
        PageTitleModule,
		ErrorPipeModule
    ],
    declarations: [IdentityComponent, DetailsComponent, EmailAddressComponent, LinkSentComponent, VerifyEmailComponent],
	providers: [IdentityFormService]
})
export class IdentityModule {}

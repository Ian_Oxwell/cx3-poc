import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Params, Router } from '@angular/router';
import { SwPush, SwUpdate, UpdateAvailableEvent } from '@angular/service-worker';
import { PageTitleService } from '@core/components/page-title/service/page-title.service';
import { Message, MessageStatus } from '@core/components/toast/model/message.model';
import { MessageService } from '@core/components/toast/service/message.service';
import { Observable, of } from 'rxjs';
import { catchError, filter, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ComponentBase } from './core/base/base.component.base';
import { HeroImageService } from './core/hero-image/service/hero-image.service';
import { IAvailabilityWidget } from './core/models/availability-widget.model';
import { ITokenState } from './identity/models/token.model';
import { IUser } from './identity/models/user.model';
import { LoginService } from './identity/service/login.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent extends ComponentBase implements OnInit {
    title = 'cx3-poc';
    isSwUpdateEnabled = false;
    isSwPushEnabled = false;
    isTokenPresent = false;
    isUserProfileLoaded = false;
    testData: IAvailabilityWidget = {
        discountCodeOnly: false,
        group: { id: 1, name: 'test', slug: 'test2', consumerWidgetGroupProducts: [] },
        products: [],
    };

    messageList: { [key: string]: Message } = {
        notificationClick: {
            severity: MessageStatus.Success,
            summary: 'Notification received',
            life: 1200,
        },
		messageReceived: {
            severity: MessageStatus.Success,
            summary: 'Notification received',
            life: 1200,
        },
    };

    publicKey: string = environment.vapidPublicKey;
    constructor(
        private http: HttpClient,
        private router: Router,
        private swUpdate: SwUpdate,
        private swPush: SwPush,
        private messageService: MessageService,
        private pageTitleService: PageTitleService,
        private loginService: LoginService,
        private heroImageService: HeroImageService,
        private actRoute: ActivatedRoute
    ) {
        super();
        this.updateClient();
    }

    ngOnInit(): void {
        // this.getApiTest().subscribe();

        // TODO add a check if the app is installed
        this.isSwPushEnabled = this.swPush.isEnabled;
        if (this.isSwUpdateEnabled) {
            this.swPush.messages
                .pipe(
                    tap((message) => {
                        this.messageService.add(this.messageList.notificationClick);
                        console.log('message received', message);
                    }),
                    takeUntil(this.ngUnsubscribe)
                )
                .subscribe();
            this.swPush.notificationClicks
                .pipe(
                    tap(({ action, notification }) => {
                        console.log('notification click', action, notification);
                        this.messageService.add({ ...this.messageList.notificationClick, detail: notification.title });
                    }),
                    takeUntil(this.ngUnsubscribe)
                )
                .subscribe();
        }

        this.pageTitleService.listenPageTitle().pipe(takeUntil(this.ngUnsubscribe)).subscribe();

        this.identityCheckLogin().subscribe();
    }

    /**
     * Identity check
     * - checks for an existing token +> get the user profile.
     * 		-> wait for the router to settle to obtain the slug
     * - navigate to account/email if slug exists and not already an account based route.
     * Note - remains active to detect changes in the tokenState.
     * @returns Observable that is not consumed.
     */
    identityCheckLogin(): Observable<string> {
        return this.loginService.getAuthentication().pipe(
            switchMap((tokenState: ITokenState | null) => {
                this.isTokenPresent = tokenState !== null;
                if (this.isTokenPresent && !this.isUserProfileLoaded) {
                    // TODO add a freshness check on the token.

                    this.loadUserProfile();
                }
                return this.router.events;
            }),
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            filter((ev: any) => ev instanceof NavigationEnd),
            switchMap((_) => (!!this.actRoute.firstChild ? this.actRoute.firstChild.params : of({}))),
            map((params: Params) => (!!params && params?.hasOwnProperty('slug') ? params['slug'] : '')),
            tap((slug: string) => {
                if (!!slug && (slug !== null || slug !== 'null')) {
                    this.heroImageService.setCurrentSlug(slug);
                    if (!this.router.url.includes('/account') && !this.isTokenPresent) {
                        this.router.navigate([`${slug}/account/email`]);
                    }
                }
            }),
            takeUntil(this.ngUnsubscribe)
        );
    }

    /** When we have a token result, get the user profile once. */
    loadUserProfile(): void {
        this.loginService
            .getSingleUserProfile()
            .pipe(
                tap((user: IUser | null) => {
                    this.isUserProfileLoaded = user !== null;
                }),
                takeUntil(this.ngUnsubscribe)
            )
            .subscribe();
    }

    /**
     * Uses SwUpdate to check if there is an update available for the PWA.
     * Only to include if we expect lots of changes in the application/assets.
     */
    updateClient(): void {
        this.isSwUpdateEnabled = this.swUpdate.isEnabled;
        if (!this.isSwUpdateEnabled) {
            return;
        }

        this.swUpdate.available
            .pipe(
                tap((event: UpdateAvailableEvent) => {
                    // TODO: change to a mat Dialog
                    if (confirm('Update available - please confirm to reload')) {
                        this.swUpdate.activateUpdate().then(() => location.reload());
                    }
                }),
                catchError((err) => {
                    console.error('sw Error', err);
                    return of();
                })
            )
            .subscribe();
    }

    /**
     * FAKE method to bypass auth for testing.
     * TODO: Remove this method
     */
    byPassAuthentication(): void {
        this.isUserProfileLoaded = true;
        this.loginService.fakeJwt();
    }

    /**
     * Gets a result from local roller db for product availability for test Trident resource.
     * @returns Observable result from API test call.
     */
    private getApiTest(): Observable<IAvailabilityWidget> {
        const httpOptions = {
            headers: new HttpHeaders({
                'x-api-key': 'trident',
                'x-cell-id': 'a',
                'x-correlationid': '0472e63f-eddc-4379-b1da-8508240bb2ec',
            }),
        };
        return this.http
            .get<IAvailabilityWidget>(
                'https://api.roller.local/api/products/availabilities/widget?endDateIndex=20210820&group=contrite&startDateIndex=20210820',
                httpOptions
            )
            .pipe(
                tap((data: IAvailabilityWidget) => {
                    this.testData = data;
                    console.log('test results', data);
                }),
                catchError((err) => {
                    console.error('test error', err);
                    return of({} as IAvailabilityWidget);
                }),
                takeUntil(this.ngUnsubscribe)
            );
    }
}

import { HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { FooterModule } from '@core/components/footer/footer.module';
import { PageTitleModule } from '@core/components/page-title/page-title.module';
import { ToastModule } from '@core/components/toast/toast.module';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeroImageModule } from './core/hero-image/hero-image.module';
import { HeroImageService } from './core/hero-image/service/hero-image.service';
import { MenuModule } from './core/menu/menu.module';
import { HomeModule } from './home/home.module';
import { LoginService } from './identity/service/login.service';
import { PromptModule } from './push-notifications/prompt/prompt.module';
import { PwaService } from './push-notifications/pwa.service';
import { RedirectModule } from './redirect/redirect.module';
import { VenueComponent } from './venue/venue.component';

const initializer = (pwaService: PwaService) => () => pwaService.initPwaPrompt(undefined);

@NgModule({
    declarations: [AppComponent, VenueComponent],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        HttpClientModule,
        ServiceWorkerModule.register('ngsw-worker.js', {
            enabled: environment.production,
            // Register the ServiceWorker as soon as the app is stable
            // or after 10 seconds (whichever comes first). (default was 30sec)
            registrationStrategy: 'registerWhenStable:10000',
        }),
        MenuModule,
        ToastModule,
        FooterModule,
        RedirectModule,
        HeroImageModule,
        PageTitleModule,
		MatButtonModule,
		HomeModule,
		PromptModule
    ],
    providers: [
        LoginService,
        HeroImageService,
        { provide: APP_INITIALIZER, useFactory: initializer, deps: [PwaService], multi: true },
    ],
    bootstrap: [AppComponent],
})
export class AppModule {}

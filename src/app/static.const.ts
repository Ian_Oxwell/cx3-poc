import { IValidationMessages } from "src/app/core/models/common.model";

// TODO Modify this to a pipe
export const ValidationMessages: IValidationMessages = {
	username: [
	  { type: 'required', message: 'Your Name is <strong>required</strong>' },
	  { type: 'minlength', message: 'Your Name is a little longer' },
	  { type: 'maxlength', message: 'Is your Name really THAT long?' },
	  { type: 'pattern', message: 'Your username must contain only numbers and letters' }
	],
	email: [
	  { type: 'required', message: 'Email is <strong>required</strong>' },
	  { type: 'minlength', message: 'Your Email is a little longer' },
	  { type: 'maxlength', message: 'Is your Email really THAT long?' },
	  { type: 'pattern', message: 'Enter a valid email' },
	  { type: 'email', message: 'Please provide a valid email address.'},
	  { type: 'googleLogin', message: 'This is a google address, please login through the Google Login button.'},
	  { type: 'registrationRequired', message: 'Email account not found, would you like to register now?'},
	  { type: 'emailConflict', message: 'Email already registered, please try resetting password?'},
	],
	password: [
	  { type: 'required', message: 'Password is <strong>required</strong>' },
	  { type: 'minlength', message: 'Your password must be a little longer.' },
	  { type: 'maxlength', message: 'Are you going to remember a password THAT long?' },
	  { type: 'pattern', message: 'Must meet a certain pattern.' },
	  { type: 'wrongPassword', message: 'Please check your password and try again.' },
	],
	phone: [
	  { type: 'required', message: 'A contact number is required' },
	  { type: 'validCountryPhone', message: 'Must be an Australian mobile or fixed line (08)' }
	],
	terms: [
	  { type: 'pattern', message: 'You must accept terms and conditions' }
	]
  };
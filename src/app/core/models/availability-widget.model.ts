export interface IConsumerWidgetGroupProduct {
    groupTitle: string;
    order: number;
    productId: number;
}

export interface IGroupAvailabilityGroup {
    id: number;
    name: string;
    slug: string;
    configuration?: {
        completeFormLater: boolean;
        discountCodeOnly: boolean;
        donationOnly: boolean;
        expandAccordions: boolean;
    };
    consumerWidgetGroupProducts: IConsumerWidgetGroupProduct[];
}

export interface IAvailabilitySessions {
    bookedQuantity: number;
    dateIndex: number;
    endTime: number;
    isSessionCapacityFull: boolean;
    locations?: string[];
    name: string;
    productId: number;
    sessionId: number;
    startTime: number;
}

export interface IAvailabilityProduct {
    allowUsage: number;
    cost: number;
    description: string;
    name: string;
    sessions: IAvailabilitySessions[];
}

export interface IAvailabilityWidget {
    discountCodeOnly: boolean;
    group: IGroupAvailabilityGroup;
    products: IAvailabilityProduct[];
}

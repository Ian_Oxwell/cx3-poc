/** Extension to strongly type ValidationErrors.errorObj */
export interface ValidationErrorObject {
	min?: number;
	max?: number;
	requiredLength?: number;
	actualLength?: number;
	message?: string;
}

/** Generic key/values */
export interface IDictionary<T> {
	[Key: string]: T;
}

/** Generic class for returned Messages from the API */
export class MessageResult {
	message: string;
	constructor(message: string) {
		this.message = message;
	}
}

export interface IValidationMessages {
	[key: string]: {
		type: string;
		message: string;
	}[];
}

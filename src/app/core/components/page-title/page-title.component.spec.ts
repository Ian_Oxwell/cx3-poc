import { ComponentFixture, TestBed } from '@angular/core/testing';
import { autoSpy, Spy } from '@tests/auto-spy';
import { PageTitleComponent } from './page-title.component';
import { PageTitleService } from './service/page-title.service';

describe('PageTitleComponent', () => {
	let component: PageTitleComponent;
	let fixture: ComponentFixture<PageTitleComponent>;

	const pageTitleServiceSpy: Spy<PageTitleService> = autoSpy(PageTitleService);

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [PageTitleComponent],
			providers: [{ provide: PageTitleService, useValue: pageTitleServiceSpy }]
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(PageTitleComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});

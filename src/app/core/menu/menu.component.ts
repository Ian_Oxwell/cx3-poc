import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ComponentBase } from '@core/base/base.component.base';
import { HeroImageService } from '@core/hero-image/service/hero-image.service';
import { Observable, timer } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { IUser } from 'src/app/identity/models/user.model';
import { LoginService } from 'src/app/identity/service/login.service';
import { UserService } from 'src/app/identity/service/user.service';
import { IMenuItem } from './menu.model';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss'],
})
export class MenuComponent extends ComponentBase implements OnInit {
    slug: string = '';
    userProfile$: Observable<IUser | null>;
	isLoggedIn$: Observable<boolean>;
    mainMenuItems: IMenuItem[] = [
        { link: `${this.slug}/`, icon: 'home', text: 'Home' },
        { link: `${this.slug}/products`, icon: 'shopping_basket', text: 'Products' },
        { link: `${this.slug}/items`, icon: 'card_giftcard', text: 'My Items' },
    ];

    // TODO Add children and click action in
    // { link: `${this.slug}/account`, icon: 'account_box', text: '', children: [
    // 	{icon: 'keyboard_return', text: 'Log out', action: this.logUserOut()}
    // ] },

    constructor(
        @Inject(DOCUMENT) private document: Document,
        private heroImageService: HeroImageService,
        private userService: UserService,
        private loginService: LoginService,
		private router: Router
    ) {
        super();
        this.userProfile$ = this.userService.userProfile;
		this.isLoggedIn$ = this.userService.isLoggedIn;
    }

    ngOnInit(): void {
        this.heroImageService.getCurrentSlug()
            .pipe(
                tap((slug: string) => (this.slug = slug)),
                takeUntil(this.ngUnsubscribe)
            )
            .subscribe();
    }

    logUserOut(): void {
        this.loginService.hardLogout();
		// soft delay for 1 sec before navigating away
		timer(1000).pipe(
			tap(_ => this.router.navigate([`${this.slug}/account/login`])),
			takeUntil(this.ngUnsubscribe)
		).subscribe();
    }

    /**
     * Scrolls the page to the main content section avoiding navigation elements.
     */
    skip(): void {
        const pageTitleElement = this.document.getElementById('pageTitleMainContent');
        if (!!pageTitleElement) {
            pageTitleElement?.scrollIntoView();
            pageTitleElement?.focus();
        }
    }

	routeTo(url: string): void {
		console.log('route to', this.slug, url);
		this.router.navigate([`${this.slug}${url}`]);
	}
}

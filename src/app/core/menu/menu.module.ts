import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { RouterModule } from '@angular/router';
import { HeroImageModule } from '../hero-image/hero-image.module';
import { MenuComponent } from './menu.component';

@NgModule({
    imports: [CommonModule, MatIconModule, RouterModule, MatButtonModule, MatMenuModule, HeroImageModule],
    declarations: [MenuComponent],
	exports: [MenuComponent]
})
export class MenuModule {}

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { RouterTestingModule } from '@angular/router/testing';
import { HeroImageComponent } from '@core/hero-image/hero-image.component';
import { HeroImageService } from '@core/hero-image/service/hero-image.service';
import { autoSpy, Spy } from '@tests/auto-spy';
import { MockComponents } from 'ng-mocks';
import { of } from 'rxjs';
import { LoginService } from 'src/app/identity/service/login.service';
import { UserService } from 'src/app/identity/service/user.service';
import { MenuComponent } from './menu.component';

describe('MenuComponent', () => {
    let component: MenuComponent;
    let fixture: ComponentFixture<MenuComponent>;

    const heroImageServiceSpy: Spy<HeroImageService> = autoSpy(HeroImageService);
    heroImageServiceSpy.getCurrentSlug.and.returnValue(of('test'));

	const userServiceSpy: Spy<UserService> = autoSpy(UserService);
	const loginServiceSpy: Spy<LoginService> = autoSpy(LoginService);


    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [RouterTestingModule, HttpClientTestingModule, MatIconModule, MatButtonModule, MatMenuModule],
            declarations: [MenuComponent, MockComponents(HeroImageComponent)],
            providers: [
				{ provide: HeroImageService, useValue: heroImageServiceSpy },
				{ provide: UserService, useValue: userServiceSpy },
				{ provide: LoginService, useValue: loginServiceSpy },
			],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(MenuComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});

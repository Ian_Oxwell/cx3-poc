
export interface IMenuItem {
	link?: string;
	icon: string;
	text: string;
	children?: IMenuItem[];
	action?: (() => void) | undefined;
}
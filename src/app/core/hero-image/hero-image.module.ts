import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { HeroImageComponent } from './hero-image.component';

@NgModule({
    imports: [CommonModule, MatIconModule],
    declarations: [HeroImageComponent],
	exports: [HeroImageComponent]
})
export class HeroImageModule {}

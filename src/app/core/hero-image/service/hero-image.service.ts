import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { IHeroSplash } from '../hero.model';

@Injectable({
    providedIn: 'root',
})
export class HeroImageService {
    heroImageSubject$: BehaviorSubject<IHeroSplash> = new BehaviorSubject<IHeroSplash>({} as IHeroSplash);
    currentSlugSubject$: BehaviorSubject<string> = new BehaviorSubject<string>('');

    getCurrentSlug(): Observable<string> {
        return this.currentSlugSubject$.asObservable();
    }
    /**
     * Gets the Hero splash details.
     * @returns Observable of Hero Splash details.
     */
    getHeroImage(): Observable<IHeroSplash> {
        return this.heroImageSubject$.asObservable();
    }

    /**
     *
     * @param hero
     */
    setHeroImage(hero: IHeroSplash) {
        this.heroImageSubject$.next(hero);
    }

    setCurrentSlug(slug: string) {
        this.currentSlugSubject$.next(slug);
    }
}

import { TestBed } from '@angular/core/testing';
import { HeroImageService } from './hero-image.service';

describe('Service: HeroImage', () => {
	let service: HeroImageService;

	beforeEach(() => {
		TestBed.configureTestingModule({});
		service = TestBed.inject(HeroImageService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});
});

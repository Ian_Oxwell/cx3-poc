
export interface IHeroSplash {
	title: string; // business name.
	subTitle: string;
	description: string;
	logo: {
		url: string;
		width: number;
		height: number;
	};
	backgroundImage: string;
}

// TODO: TEMP do not use in production
export interface IHeroSplashCollection {
	[key: string]: IHeroSplash;
}
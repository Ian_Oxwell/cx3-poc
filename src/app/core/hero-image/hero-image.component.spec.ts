import { ComponentFixture, TestBed } from '@angular/core/testing';
import { autoSpy, Spy } from '@tests/auto-spy';
import { of } from 'rxjs';
import { HeroImageComponent } from './hero-image.component';
import { IHeroSplash } from './hero.model';
import { HeroImageService } from './service/hero-image.service';

describe('HeroImageComponent', () => {
    let component: HeroImageComponent;
    let fixture: ComponentFixture<HeroImageComponent>;

    const heroImageServiceSpy: Spy<HeroImageService> = autoSpy(HeroImageService);
    heroImageServiceSpy.getHeroImage.and.returnValue(of({} as IHeroSplash));
    heroImageServiceSpy.getCurrentSlug.and.returnValue(of('test'));

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [],
            declarations: [HeroImageComponent],
            providers: [{ provide: HeroImageService, useValue: heroImageServiceSpy }],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(HeroImageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});

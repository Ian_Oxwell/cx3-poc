import { Component, OnInit } from '@angular/core';
import { ComponentBase } from '@base/base.component.base';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map, takeUntil } from 'rxjs/operators';
import { IHeroSplash, IHeroSplashCollection } from './hero.model';
import { HeroImageService } from './service/hero-image.service';

@Component({
    selector: 'app-hero-image',
    templateUrl: './hero-image.component.html',
    styleUrls: ['./hero-image.component.scss'],
})
export class HeroImageComponent extends ComponentBase implements OnInit {
    heroSplash$: Observable<IHeroSplash | undefined>;

    // TODO: replace with an API that gets data based on the slug.
    heroImages: IHeroSplashCollection = {
        'cc-adventure-park': {
            title: 'Christ Church Adventure Park',
            subTitle: 'Your adventure starts here',
            description:
                'Your base for epic mountain bike, zipline & sightseeing adventures as well as chilled out down-time in Christchurch’s Port Hills',
            logo: {
                width: 81.57,
                height: 80,
                url: '/assets/site-images/ccadventure-logo-white.png',
            },
            backgroundImage: '/assets/site-images/eerik-sandstrom-MQwQk93YISE-unsplash.jpg',
        },
        thredbo: {
            title: 'Thredbo',
            subTitle: 'Winter is here',
            description: 'It’s time to carve up the slopes. We have a package for every winter adventure.',
            logo: {
                width: 62,
                height: 48,
                url: '/assets/site-images/thredbo-logo.png',
            },
            backgroundImage: '/assets/site-images/Torah-pow-turn-5L7A8589-photo-credit-Thredbo-1-1920x550.jpg',
        },
    };

    constructor(private heroImageService: HeroImageService) {
        super();
        this.heroSplash$ = this.heroImageService.getHeroImage().pipe(
            map((hero: IHeroSplash) => (Object.keys(hero).length > 0 ? hero : undefined)),
            takeUntil(this.ngUnsubscribe)
        );
    }

    ngOnInit(): void {
		this.listenSlugEvents().subscribe();
    }

	/**
	 * Listens for current Slug changes.
	 * Slug is observed and set in app.component for route changes.
	 * @returns Observable of Current Hero Splash details.
	 */
	private listenSlugEvents(): Observable<IHeroSplash> {
		return this.heroImageService.getCurrentSlug().pipe(
			distinctUntilChanged(),
			map((slug: string) => {
				return this.setHeroImage(slug);
			}),
			takeUntil(this.ngUnsubscribe)
		)
	}

	/**
	 * Listens for router navigation End in order to query to route params for the slug variable.
	 * Sets hero
	 */
	// private listenRouterEvents(): void {
	// 	this.router.events.pipe(
	// 		// only listen for nav end
	// 		// eslint-disable-next-line @typescript-eslint/no-explicit-any
	// 		filter((event: any) => {
	// 			return event instanceof NavigationEnd;
	// 		}),
	// 		switchMap(_ => !!this.actRoute.firstChild ? this.actRoute.firstChild.params : of({})),
	// 		map((params: Params) => (!!params && params?.hasOwnProperty('slug')) ? params['slug'] : ''),
	// 		tap((slug: string | null) => {
	// 			this.heroImageService.setCurrentSlug(slug as string);
	// 			this.setHeroImage(slug);
	// 		}),
	// 		takeUntil(this.ngUnsubscribe)
	// 	).subscribe();
	// }

	/**
	 * Sets the hero image in the service for other components to consume.
	 * @param slug string to set the 
	 */
	private setHeroImage(slug: string | null): IHeroSplash {
		// TODO: ensure these results are coming from the API.
		const heroImage: IHeroSplash =
			typeof slug === 'string' && !!this.heroImages[slug]
				? this.heroImages[slug]
				: ({} as IHeroSplash);
		this.heroImageService.setHeroImage(heroImage);
		return heroImage;
	}
}

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SwPush, SwUpdate, UpdateActivatedEvent, UpdateAvailableEvent } from '@angular/service-worker';
import { FooterComponent } from '@core/components/footer/footer.component';
import { MessageService } from '@core/components/toast/service/message.service';
import { ToastComponent } from '@core/components/toast/toast.component';
import { SwPushServerMock } from '@tests/service-worker/sw-push-server.mock.service';
import { MockComponents } from 'ng-mocks';
import { of, Subject } from 'rxjs';
import { AppComponent } from './app.component';
import { autoSpy, Spy } from './tests/auto-spy';
import { SwUpdateServerMock } from './tests/service-worker/sw-update-server.mock.service';

describe('AppComponent', () => {
    let component: AppComponent;
    let fixture: ComponentFixture<AppComponent>;

    const messageServiceSpy: Spy<MessageService> = autoSpy(MessageService);

    const swUpdateSpy: Spy<SwUpdateServerMock> = autoSpy(SwUpdateServerMock);
    const updateAvailable: UpdateAvailableEvent = {
        type: 'UPDATE_AVAILABLE',
        current: { hash: '123Test ' },
        available: { hash: '124Test' },
    };

    const swPushSpy: Spy<SwPushServerMock> = autoSpy(SwPushServerMock);

    let messageTest: Subject<object> = new Subject();
    swPushSpy.messages = messageTest.asObservable();
    swPushSpy.notificationClicks = of();

    // let updates: Subject<UpdateAvailableEvent> = new Subject<UpdateAvailableEvent>();
    // swUpdateSpy.available = updates;

    let activated: Subject<UpdateActivatedEvent>;
    activated = new Subject<UpdateActivatedEvent>();
    swUpdateSpy.activated = activated.asObservable();

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [RouterTestingModule, HttpClientTestingModule],
            declarations: [AppComponent, MockComponents(FooterComponent, ToastComponent)],
            providers: [
                { provide: SwUpdate, useValue: swUpdateSpy },
                { provide: SwPush, useValue: swPushSpy },
                { provide: MessageService, useValue: messageServiceSpy },
            ],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(AppComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create the app', () => {
        expect(component).toBeTruthy();
    });

    describe('PWA update available', () => {
        let updates: Subject<UpdateAvailableEvent>;

        beforeEach(() => {
            updates = new Subject<UpdateAvailableEvent>();
            swUpdateSpy.available = updates.asObservable();
        });

        it('should set UpdateEnabled to false if swUpdate is NOT enabled', () => {
            component.isSwUpdateEnabled = true;
            swUpdateSpy.isEnabled = false;
            component.updateClient();
            expect(component.isSwUpdateEnabled).toBeFalsy();
        });

        it('should set UpdateEnabled to true if swUpdate IS enabled', () => {
            component.isSwUpdateEnabled = false;
            swUpdateSpy.isEnabled = true;

            component.updateClient();
            expect(component.isSwUpdateEnabled).toBeTrue();
        });

        it('should set ask to confirm a reload if update is available', () => {
            swUpdateSpy.isEnabled = true;

            const confirmSpy = spyOn(window, 'confirm').and.returnValue(false);

            component.updateClient();
            updates.next(updateAvailable);
            expect(confirmSpy).toHaveBeenCalled();
        });
    });
});

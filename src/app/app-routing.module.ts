import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProductComponent } from './product/product.component';
import { RedirectComponent } from './redirect/redirect.component';
import { VenueComponent } from './venue/venue.component';

const identityModule = () => import('./identity/identity.module').then((x) => x.IdentityModule);

const routes: Routes = [
    {
        path: ':slug',
		component: VenueComponent,
        children: [
            {
                path: 'account',
                loadChildren: identityModule,
            },
			{
				path: '',
				component: HomeComponent,
				data: { title: 'Home' }
			},
			{
				path: 'product',
				component: ProductComponent,
				data: { title: 'Products' }
			}
        ]
    },
	{ path: '', component: RedirectComponent, data: {title: 'Redirect'} },
	{ path: '**', redirectTo: '' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}

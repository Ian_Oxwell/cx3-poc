export interface IPushSubscription {
	deviceName: string;
	endPoint: string;
	keyP256dh: string;
	keyAuth: string;
	userId?: number;
	id?: number;
}
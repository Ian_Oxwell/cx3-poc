import { HttpClientTestingModule } from '@angular/common/http/testing';
import { inject, TestBed } from '@angular/core/testing';
import { PushService } from './push.service';

describe('Service: Push', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [PushService],
        });
    });

    it('should ...', inject([PushService], (service: PushService) => {
        expect(service).toBeTruthy();
    }));
});

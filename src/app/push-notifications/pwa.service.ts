/**
 * Based on
 * https://medium.com/@oleksandr.k/a-way-to-show-a-prompt-to-install-your-angular-pwa-both-on-android-and-ios-devices-7a770f55c54
 * and
 * https://github.com/docluv/add-to-homescreen/blob/master/src/addtohomescreen.js
 */
import { Platform } from '@angular/cdk/platform';
import { Injectable } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { timer } from 'rxjs';
import { take } from 'rxjs/operators';
import { PromptComponent } from './prompt/prompt.component';

@Injectable({
    providedIn: 'root',
})
export class PwaService {
    private promptEvent: any;
    constructor(private platform: Platform, private bottomSheet: MatBottomSheet) {}

    checkServiceWorker(): void {
        //if no service worker then no add to homescreen
        if ('serviceWorker' in window.navigator) {
            console.log('service worker present');
            const manifestEle = document.querySelector("[rel='manifest']");
            //if no manifest file then no add to homescreen
            if (!manifestEle) {
                console.log('no manifest file, add to homescreen not displaying');
            } else {
                setTimeout(() => navigator.serviceWorker.getRegistration().then(this.initPwaPrompt), 5000) ;
				// this.initPwaPrompt(undefined);
            }
        }
    }

    public initPwaPrompt(serviceW: ServiceWorkerRegistration | undefined): void {
        if (!serviceW) {
            console.log('no service worker registered', serviceW);
            //return, no need to go further
            // return;
        }
        if (this.platform.ANDROID || this.platform.BLINK) {
			console.log('must be blink');
            window.addEventListener('beforeinstallprompt', (event: any) => {
				console.log('before install triggered', event);
                event.preventDefault();
                this.promptEvent = event;
                this.openPromptComponent('android');
            });
        } else if (this.platform.IOS) {
            const isInStandaloneMode =
                'standalone' in window.navigator && window.navigator.hasOwnProperty('standalone');
            if (!isInStandaloneMode) {
                this.openPromptComponent('ios');
            }
        }
    }

    private openPromptComponent(mobileType: 'ios' | 'android') {
        timer(3000)
            .pipe(take(1))
            .subscribe(() =>
                this.bottomSheet.open(PromptComponent, { data: { mobileType, promptEvent: this.promptEvent } })
            );
    }
}

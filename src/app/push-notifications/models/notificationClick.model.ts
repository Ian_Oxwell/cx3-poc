export interface INotificationClick {
	action: string;
	notification: NotificationOptions & {
		title: string;
	};
}
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatBottomSheetModule, MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';
import { PromptComponent } from './prompt.component';

describe('PromptComponent', () => {
    let component: PromptComponent;
    let fixture: ComponentFixture<PromptComponent>;

    // const bottomSheetSpy: Spy<MAT_BOTTOM_SHEET_DATA> = autoSpy(MAT_BOTTOM_SHEET_DATA);
    const bottomData = { data: { mobileType: 'android' } };
    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [MatBottomSheetModule],
            declarations: [PromptComponent],
            providers: [
                { provide: MAT_BOTTOM_SHEET_DATA, useValue: bottomData },
                { provide: MatBottomSheetRef, useValue: PromptComponent },
            ],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(PromptComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});

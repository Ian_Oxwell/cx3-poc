import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { PromptComponent } from './prompt.component';

@NgModule({
    imports: [CommonModule, MatToolbarModule, MatButtonModule, MatIconModule, MatBottomSheetModule],
    declarations: [PromptComponent],
    entryComponents: [PromptComponent],
})
export class PromptModule {}

export interface IPushNotice {
	id: number;
	notification: string;
}
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IPushNotice } from './push-notice.model';
import { IPushSubscription } from './push-subscription.model';

@Injectable({
    providedIn: 'root',
})
export class PushService {
    private baseUrl = `${environment.apiUrl}/push`;

    constructor(private http: HttpClient) {}

    /**
     * Add a new device/subscription for push notifications for current user.
     * @param push Details to register device to the currently logged in user.
     * @returns Number of the device in the DB.
     */
    addPushSubscriber(subscription: PushSubscription): Observable<number> {
		const rawKey: ArrayBuffer | string | null = subscription.getKey ? subscription.getKey('p256dh') : '';
		const rawKeyTypedArray = new Uint8Array(rawKey as ArrayBuffer);
		const keyP256dh = rawKey ? btoa(String.fromCharCode.apply(null, [...rawKeyTypedArray])) : '';
		const rawAuthSecret = subscription.getKey ? subscription.getKey('auth') : '';
		const rawAuthTypedArray = new Uint8Array(rawAuthSecret as ArrayBuffer);
		const keyAuth = rawAuthSecret
			? btoa(String.fromCharCode.apply(null, [...rawAuthTypedArray]))
			: '';
		const endPoint = subscription.endpoint;
		const pushNotificationSubscription: IPushSubscription = {
			deviceName: 'Test-Vlad',
			keyP256dh,
			endPoint,
			keyAuth,
			userId: 1
		};

		console.log('push subscriptions details', subscription);
        return this.http.post<number>(`${this.baseUrl}/subscription`, pushNotificationSubscription);
    }

    /**
     * Attempts to push a new notice to a specified Device Id.
     * @param notice The device Id and the notice payload.
     * @returns string message of success or failure.
     */
    pushNotice(notice: IPushNotice): Observable<string> {
        return this.http.post<string>(`${this.baseUrl}/notice`, notice);
    }
}

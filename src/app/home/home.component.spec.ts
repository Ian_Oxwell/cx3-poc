import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { SwPush } from '@angular/service-worker';
import { MessageService } from '@core/components/toast/service/message.service';
import { ErrorkeysPipe } from '@core/form/error-pipe/error-keys.pipe';
import { FormErrorMessagePipe } from '@core/form/error-pipe/form-error-message.pipe';
import { autoSpy, Spy } from '@tests/auto-spy';
import { SwPushServerMock } from '@tests/service-worker/sw-push-server.mock.service';
import { MockPipes } from 'ng-mocks';
import { PushService } from '../push-notifications/push.service';
import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
    let component: HomeComponent;
    let fixture: ComponentFixture<HomeComponent>;

    const pushServiceSpy: Spy<PushService> = autoSpy(PushService);
    const swPushSpy: Spy<SwPushServerMock> = autoSpy(SwPushServerMock);
    const messageServiceSpy: Spy<MessageService> = autoSpy(MessageService);
    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [
                ReactiveFormsModule,
				MatButtonModule,
                MatFormFieldModule,
                MatInputModule,
                MatIconModule,
                HttpClientTestingModule,
                RouterTestingModule,
				NoopAnimationsModule
            ],
            declarations: [HomeComponent, MockPipes(ErrorkeysPipe, FormErrorMessagePipe)],
            providers: [
                { provide: SwPush, useValue: swPushSpy },
                { provide: PushService, useValue: pushServiceSpy },
                { provide: MessageService, useValue: messageServiceSpy },
                FormBuilder,
            ],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(HomeComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});

import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SwPush } from '@angular/service-worker';
import { ComponentBase } from '@core/base/base.component.base';
import { Message, MessageStatus } from '@core/components/toast/model/message.model';
import { MessageService } from '@core/components/toast/service/message.service';
import { environment } from '@env/environment';
import { timer } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { IFormModel } from '../identity/models/identity-forms.model';
import { PushService } from '../push-notifications/push.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
})
export class HomeComponent extends ComponentBase implements OnInit {
    publicKey: string = environment.vapidPublicKey;
    isSwPushEnabled = false;
    formModel: IFormModel = {
        title: ['New Product Available', [Validators.required, Validators.minLength(2), Validators.maxLength(120)]],
        text: [
            'Take a look at this brand new t-shirt!',
            [Validators.required, Validators.minLength(2), Validators.maxLength(120)],
        ],
    };
    form: FormGroup;

    messageNotRegistered: Message = {
        severity: MessageStatus.Alert,
        summary: 'Service Worker not registered',
        life: 12000,
    };

    messageNowRegistered: Message = {
        severity: MessageStatus.Success,
        summary: 'Service Worker registered',
        life: 12000,
    };

    messageFailRegister: Message = {
        severity: MessageStatus.Error,
        summary: 'Service Worker failed to register',
        life: 12000,
    };

    constructor(
        private pushService: PushService,
        private swPush: SwPush,
        private fb: FormBuilder,
        private messageService: MessageService,
		private router: Router
    ) {
        super();
        this.form = this.createForm(this.formModel);
    }

    ngOnInit(): void {
        this.isSwPushEnabled = this.swPush.isEnabled;
    }

    /** Quick create Form */
    createForm(formModel: IFormModel): FormGroup {
        return this.fb.group(formModel);
    }

    notificationPermission(): void {
		if ('Notification' in window){
			console.log('notification permission', Notification.permission);
			this.messageService.add({
				severity: MessageStatus.Alert,
				summary: 'Notification permission',
				detail: Notification.permission,
				life: 12000,
			});
			if (Notification.permission !== 'granted') {
				Notification.requestPermission().then((permission: NotificationPermission) => {
					this.messageService.add({
						severity: permission === 'granted' ? MessageStatus.Success : MessageStatus.Alert,
						summary: 'Notification permission',
						detail: permission,
						life: 12000,
					});
				});
			}
		} else {
			this.messageService.add({
				severity: MessageStatus.Warning,
				summary: 'Notification is not allowed on this device',
				life: 12000,
			});
		}

    }

    /**
     * Fake send notification
     * https://notifications.spec.whatwg.org/#api
     */
    sendNoticeTest(): void {
        if (!this.form.valid) {
            return;
        }
        if (!this.isSwPushEnabled) {
            this.messageService.add(this.messageNotRegistered);
            this.swPush
                .requestSubscription({
                    serverPublicKey: this.publicKey,
                })
                .then((value) => {
                    this.messageService.add(this.messageNowRegistered);
                })
                .catch((value) => {
                    this.messageService.add(this.messageFailRegister);
                });
        }

        const rawForm = this.form.getRawValue();
        const img = '/assets/site-images/Torah-pow-turn-5L7A8589-photo-credit-Thredbo-1-1920x550.jpg';
        const text = rawForm.text;
        const title = rawForm.title;
        const options = {
            body: text,
            icon: '/assets/site-images/thredbo-logo.png',
            vibrate: [200, 100, 200],
            tag: 'new-product',
            image: img,
            badge: 'https://spyna.it/icons/android-icon-192x192.png',
            actions: [{ action: 'Detail', title: 'View', icon: 'https://via.placeholder.com/128/ff0000' }],
        };
        navigator.serviceWorker.ready.then(function (serviceWorker) {
            console.log('send to service worker', serviceWorker);
            serviceWorker.showNotification(title, options).then(() => console.log('it has sent...'));
        });
    }

    /**
     * Sends notice in form to API - does not work on mobile.
     */
    sendNoticeApi(): void {
        console.log('not yet implemented');
        const url = 'push/notice';
    }

    /**
     * Triggered from template - firing on user request.
     */
    pushSubscription(): void {
        // this.isSwPushEnabled = this.swPush.isEnabled;
        // if (!this.swPush.isEnabled) {
        //     console.log('not enabled for push');
        //     return;
        // }
        console.log('testing push subscription', this.publicKey);
        this.swPush
            .requestSubscription({
                serverPublicKey: this.publicKey,
            })
            .then((subscription: PushSubscription) => this.pushService.addPushSubscriber(subscription).subscribe())
            .catch((err: HttpErrorResponse) => console.error);
    }

	    /**
     * Small method to engage navigation to url with a delay.
     * @param url Url to navigate to - no need to include slug `/example'
     * @param delayMilliseconds soft delay before navigating.
     */
		  navigateToUrl(url: string, delayMilliseconds: number): void {
			timer(delayMilliseconds)
				.pipe(
					tap((_) => this.router.navigate([`/${url}`])),
					takeUntil(this.ngUnsubscribe)
				)
				.subscribe();
		}
}

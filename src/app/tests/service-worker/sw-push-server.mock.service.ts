/** https://github.com/maciejtreder/ng-toolkit/blob/master/application/src/app/services/swUpdate-server.mock.service.ts */

import { Observable, of } from 'rxjs';
import { INotificationClick } from 'src/app/push-notifications/models/notificationClick.model';

export class SwPushServerMock {
    public messages: Observable<object> = of();
    public subscription: Observable<PushSubscription | null> = of();

	public notificationClicks: Observable<INotificationClick> = of();
    public requestSubscription(options: { serverPublicKey: string }): Promise<PushSubscription | null> {
        return new Promise((resolve) => resolve(null));
    }
    public unsubscribe(): Promise<void> {
        return new Promise((resolve) => resolve());
    }
}

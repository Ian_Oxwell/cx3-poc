/** https://github.com/maciejtreder/ng-toolkit/blob/master/application/src/app/services/swUpdate-server.mock.service.ts */

import { UpdateActivatedEvent, UpdateAvailableEvent } from '@angular/service-worker';
import { Observable, Subject } from 'rxjs';

export class SwUpdateServerMock {
    // these are read only in SwUpdateService
    public available: Observable<UpdateAvailableEvent> = new Subject();
    public activated: Observable<UpdateActivatedEvent> = new Subject();
    public isEnabled: boolean = false;

    public checkForUpdate(): Promise<void> {
        return new Promise((resolve) => resolve());
    }
    public activateUpdate(): Promise<void> {
        return new Promise((resolve) => resolve());
    }
}

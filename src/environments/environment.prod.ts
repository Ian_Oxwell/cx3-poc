export const environment = {
  production: true,
  vapidPublicKey: 'BOPZVJ9roE_7xMKRGYoFuvWU0ZCgENldJuv8X9auiy26VPDtcT-5yeHSNTT3AGfSfKTDOWUoML4Db_zhvJeA5xE',
  apiUrl: 'https://localhost:44393/api',
  appTitle: 'CX3 POC',
  baseTitle: 'Customer',
  defaultRoute: '/'
};

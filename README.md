# Cx3Poc

This project is a proof of concept for PWA service worker.
[can I Use](https://caniuse.com/serviceworkers)<br/>
[Service Workers](https://web.dev/progressive-web-apps/)<br/>

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. `ng build` automatically adds --prod<br/>
Run `firebase deploy` to push to firebase.


## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

using Web.Api.Configuration.models;

namespace Web.Api.Configuration
{
    /// <summary>
    /// Config object must be added as scoped to the DI container for IOptionsSnapshot
    /// to work and flow config changes through to consuming services.
    /// Note that this means consuming services cannot be singletons.
    /// </summary>
    public interface IAppConfiguration
    {
        /// <summary>
        /// Gets the application settings.
        /// </summary>
        /// <value>
        /// The application settings.
        /// </value>
        AppSetting AppSettings { get; }
    }
}

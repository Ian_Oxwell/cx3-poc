using Microsoft.Extensions.Options;
using Web.Api.Configuration.models;

namespace Web.Api.Configuration
{
    /// <inheritdoc />
    public class AppConfiguration : IAppConfiguration
    {
        /// <inheritdoc />
        public AppSetting AppSettings { get; }

        /// <summary>
        /// Note options snapshot to allow appsettings.json changes to flow in while the app is running.
        /// </summary>
        /// <param name="appSettings">The application settings.</param>
        /// <param name="connectionStrings">The connection strings.</param>
        public AppConfiguration(IOptionsSnapshot<AppSetting> appSettings,
            IOptionsSnapshot<ConnectionStrings> connectionStrings,
            IOptionsSnapshot<JwtIssuerOptions> jwtIssuerOptions,
            IOptionsSnapshot<VapidServerKeys> vapidServerKeys)
        {
            AppSettings = appSettings.Value;
            AppSettings.ConnectionStrings = connectionStrings.Value;
            AppSettings.JwtIssuerOptions = jwtIssuerOptions.Value;
            AppSettings.VapidServerKeys = vapidServerKeys.Value;
        }
    }
}

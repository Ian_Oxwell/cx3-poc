namespace Web.Api.Configuration.models
{
    public class AppSetting
    {
        public virtual ConnectionStrings ConnectionStrings { get; set; }
        public virtual JwtIssuerOptions JwtIssuerOptions { get; set; }
        public virtual VapidServerKeys VapidServerKeys { get; set; }
    }
}

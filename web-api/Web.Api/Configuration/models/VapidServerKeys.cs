namespace Web.Api.Configuration.models
{
    /// <summary>
    /// VAPID stands for Voluntary Application Server Identification for Web Push protocol.
    /// A VAPID key pair is a cryptographic public/private key pair.
    /// </summary>
    public class VapidServerKeys
    {
        /// <summary>
        /// Gets or sets the Vapid Public Key.
        /// </summary>
        public string PublicKey { get; set; }

        /// <summary>
        /// Gets or sets the Vapid Private Key.
        /// </summary>
        public string PrivateKey { get; set; }

        /// <summary>
        /// Gets or sets the Subject.
        /// </summary>
        public string Subject { get; set; }
    }
}

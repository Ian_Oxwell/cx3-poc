using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Api.Configuration.models
{
    public class JwtIssuerOptions
    {
        public virtual string Issuer { get; set; }

        public virtual string Audience { get; set; }

        public virtual int? ClockSkew { get; set; }

        public virtual string Key { get; set; }
        public virtual int JwtLifetime { get; set; }
    }
}

using System;
using System.ComponentModel.DataAnnotations.Schema;
using Web.Api.CommonModels;

namespace Web.Api.Data.Models
{
    [Table("PwaSubscription", Schema = "sec")]
    public partial class PwaSubscription : BaseModel
    {
        /// <summary>
        /// Gets or sets the friendly device name (only needed for testing).
        /// </summary>
        public string DeviceName { get; set; }

        /// <summary>
        /// Gets or sets the Google Api endpoint to call.
        /// </summary>
        public string EndPoint { get; set; }

        /// <summary>
        /// Gets or sets the p256dh key.
        /// </summary>
        public string KeyP256dh { get; set; }

        /// <summary>
        /// Gets or sets the Authorisation Key.
        /// </summary>
        public string KeyAuth { get; set; }

        /// <summary>
        /// Gets or sets the Created date.
        /// </summary>
        public DateTimeOffset CreatedAt { get; set; }

        /// <summary>
        /// Gets or sets the parent UserId.
        /// </summary>
        public int UserId { get; set; }
    }
}

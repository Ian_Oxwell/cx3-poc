using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using Web.Api.CommonModels;

namespace Web.Api.Data.Models
{
    [Table("User", Schema = "sec")]
    public class User : BaseModel
    {
        /// <summary>
        /// Gets or sets the given names.
        /// </summary>
        public string GivenNames { get; set; }

        /// <summary>
        /// Gets or sets the family name.
        /// </summary>
        public string FamilyName { get; set; }

        /// <summary>
        /// Gets or sets the Date time object created.
        /// </summary>
        public DateTimeOffset CreatedAt { get; set; }


        /// <summary>
        /// Gets or sets the user's name formatted as "FirstName LastName".
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        [Required]
        [MaxLength(200)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the phone number.
        /// </summary>
        [AllowNull]
        [MaxLength(20)]
        public string MobileNumber { get; set; }

        /// <summary>
        /// Gets or sets the collection of Refresh Tokens.
        /// </summary>
        public virtual List<RefreshToken> RefreshTokens { get; set; }

        /// <summary>
        /// Gets or sets the collection of subscribed Web Applications.
        /// </summary>
        public virtual IEnumerable<PwaSubscription> PwaSubscriptions { get; set; }
    }
}

using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Api.Data.Models;

namespace Web.Api.Data
{
    /// <summary>
    /// The database context to operate in.
    /// </summary>
    public partial class ApplicationDbContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationDbContext"/> class.
        /// </summary>
        public ApplicationDbContext()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationDbContext"/> class.
        /// </summary>
        /// <param name="options">The options to configure the context with.</param>
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        /// <summary>
        /// Gets or sets the DB Context for User.
        /// </summary>
        public virtual DbSet<User> User { get; set; }

        /// <summary>
        /// Gets or sets the DB Context for PWA Subscriptions.
        /// </summary>
        public virtual DbSet<PwaSubscription> PwaSubscription { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.DisplayName).HasComputedColumnSql("[GivenNames] + ' ' + [FamilyName]");
                entity.Property(e => e.CreatedAt).HasDefaultValueSql("(sysdatetimeoffset())");
                entity.Property(e => e.RowVer).IsRowVersion();
            });

            modelBuilder.Entity<PwaSubscription>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasDefaultValueSql("(sysdatetimeoffset())");
                entity.Property(e => e.RowVer).IsRowVersion();
            });
        }
    }
}

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Pcb.Api.Auth;
using System;
using System.Text;
using Web.Api.Configuration;
using Web.Api.Configuration.models;
using Web.Api.Data;
using Web.Api.Mapping;
using Web.Api.Service.PushNotification;
using Web.Api.Service.Token;
using Web.Api.Service.UserServices;

namespace Web.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container..
        /// </summary>
        /// <param name="services">The DI service collection.</param>
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDbContext<ApplicationDbContext>
                (options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options =>
                {
                    var jwtConfig = Configuration.GetSection("JwtIssuerOptions").Get<JwtIssuerOptions>();
                    var signingKey = Encoding.UTF8.GetBytes(jwtConfig.Key);

                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateAudience = true,
                        ValidAudience = jwtConfig.Audience,
                        ValidateIssuer = true,
                        ValidIssuer = jwtConfig.Issuer,
                        RequireExpirationTime = true,
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(signingKey),
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.FromMinutes(jwtConfig.ClockSkew ?? 5),
                    };
                });

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "POC CX PWA", Version = "v1" });
            });

            // Add Context Accessors so we can inject and access the current logged in user's claims.
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // Configuration Options.
            services.Configure<ConnectionStrings>(options => Configuration.GetSection("ConnectionStrings").Bind(options));
            services.Configure<JwtIssuerOptions>(options => Configuration.GetSection("JwtIssuerOptions").Bind(options));
            services.Configure<VapidServerKeys>(options => Configuration.GetSection("VapidServerKeys").Bind(options));
            services.AddScoped<IAppConfiguration, AppConfiguration>();

            // Add Auto Mapper
            services.AddAutoMapper(typeof(AppMappings));

            // Add Scoped Services
            services.AddScoped<ITokenService, TokenService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IPushNotificationService, PushNotificationService>();
            services.AddScoped<IJwtFactory, JwtFactory>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "POC CX PWA v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

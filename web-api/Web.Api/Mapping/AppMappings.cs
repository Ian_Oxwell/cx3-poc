using AutoMapper;
using Web.Api.Data.Models;
using Web.Api.Dto.PushNotification;
using Web.Api.Dto.User;

namespace Web.Api.Mapping
{
    public class AppMappings : Profile
    {
        public AppMappings()
        {
            CreateMap<PwaSubscription, PwaSubscriptionKeyDto>().ReverseMap();
            CreateMap<User, UserProfileDto>().ReverseMap();
            CreateMap<User, RegisterUserDto>().ReverseMap();
        }
    }
}

using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Web.Api.Configuration;

namespace Pcb.Api.Auth
{
    /// <inheritdoc />
    public class JwtFactory : IJwtFactory
    {
        /// <summary>
        /// The App configuration instance
        /// </summary>
        private readonly IAppConfiguration Config;

        /// <summary>
        /// JWT Factory Interface constructor.
        /// </summary>
        /// <param name="config">The configuration instance.</param>
        public JwtFactory(IAppConfiguration config)
        {
            Config = config;
        }

        /// <inheritdoc />
        public string GenerateToken(IEnumerable<Claim> claims)
        {
            var audience = Config.AppSettings.JwtIssuerOptions.Audience;
            var issuer = Config.AppSettings.JwtIssuerOptions.Issuer;
            var startDate = DateTime.Now.ToUniversalTime();
            var endDate = DateTime.Now.AddMinutes(Config.AppSettings.JwtIssuerOptions.JwtLifetime).ToUniversalTime();

            string secureKey = Config.AppSettings.JwtIssuerOptions.Key;
            var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secureKey));
            var signingCredential = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256);

            var jwtHeader = new JwtHeader(signingCredential);
            var jwtPayload = new JwtPayload(issuer, audience, claims, startDate, endDate, startDate);
            var token = new JwtSecurityToken(jwtHeader, jwtPayload);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}

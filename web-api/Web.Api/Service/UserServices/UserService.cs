using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Api.Data;
using Web.Api.Data.Models;
using Web.Api.Dto.PushNotification;
using Web.Api.Dto.Token;
using Web.Api.Dto.User;

namespace Web.Api.Service.UserServices
{
    public class UserService : IUserService
    {
        private readonly ApplicationDbContext _db;
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserService"/> class.
        /// </summary>
        /// <param name="db">Database Context.</param>
        /// <param name="mapper">Automapper instance.</param>
        public UserService(ApplicationDbContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        /// <inheritdoc />
        public async Task<UserProfileDto> GetSingleUser(int userId)
        {
            if (userId == 0)
            {
                return null;
            }


            var user = await _db.User.Where(u => u.Id == userId)
                .Include(x => x.PwaSubscriptions)
                .SingleOrDefaultAsync();
            if (user == null)
            {
                return null;
            }

            UserProfileDto userMap = _mapper.Map<UserProfileDto>(user);
            userMap.SubscriptionKeys = _mapper.Map<IEnumerable<PwaSubscription>, IEnumerable<PwaSubscriptionKeyDto>>(user.PwaSubscriptions);

            return userMap;
        }

        /// <inheritdoc />
        public Task<bool> RegisterPwaSubscription(RegisterPwaSubscriptionDto model)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public async Task<int> RegisterUser(RegisterUserDto userDto)
        {
            User userMap = _mapper.Map<User>(userDto);
            _db.User.Add(userMap);
            await _db.SaveChangesAsync();


            return userMap.Id;
        }

        /// <inheritdoc />
        public bool UserNameAvailable(string email)
        {
            User value = _db.User.FirstOrDefault(a => a.Email.ToLower().Trim() == email.ToLower().Trim());
            return (value == null);
        }

        /// <inheritdoc />
        public Task<bool> ValidateToken(ValidateTokenRequestDto model)
        {
            throw new NotImplementedException();
        }
    }
}

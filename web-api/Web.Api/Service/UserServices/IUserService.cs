using System.Threading.Tasks;
using Web.Api.Dto.PushNotification;
using Web.Api.Dto.Token;
using Web.Api.Dto.User;

namespace Web.Api.Service.UserServices
{
    public interface IUserService
    {
        /// <summary>
        /// Checks if the email address has already been registered in the DB.
        /// </summary>
        /// <param name="email">Email address to check.</param>
        /// <returns>True if the username is available - not found in DB</returns>
        bool UserNameAvailable(string email);

        /// <summary>
        /// Registers a new user account - against email address.
        /// </summary>
        /// <param name="userDto">Shortened user Dto to register with,</param>
        /// <returns>Id of the newly created User.</returns>
        Task<int> RegisterUser(RegisterUserDto userDto);

        /// <summary>
        /// Gets a single user account from Id.
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns>User Profile</returns>
        Task<UserProfileDto> GetSingleUser(int userId);

        /// <summary>
        /// Validates an existing token, checks expiry time.
        /// </summary>
        /// <param name="model">The token model to check.</param>
        /// <returns>True for a Valid token.</returns>
        Task<bool> ValidateToken(ValidateTokenRequestDto model);

        /// <summary>
        /// Registers a new PWA subscription for messaging against a user, checks if subscription already exists.
        /// </summary>
        /// <param name="model">the device number and unique key.</param>
        /// <returns>True for a successful registration.</returns>
        Task<bool> RegisterPwaSubscription(RegisterPwaSubscriptionDto model);
    }
}

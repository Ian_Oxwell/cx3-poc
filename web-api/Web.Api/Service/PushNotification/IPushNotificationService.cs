using System.Threading.Tasks;
using Web.Api.Dto.PushNotification;

namespace Web.Api.Service.PushNotification
{
    public interface IPushNotificationService
    {
        /// <summary>
        /// Registers a new PWA device - against user id.
        /// </summary>
        /// <param name="pushKey">Shortened user Dto to register with.</param>
        /// <returns>Id of the newly created device subscription.</returns>
        Task<int> RegisterDevice(RegisterPwaSubscriptionDto pushKey);

        /// <summary>
        /// Push a notification of type string to registered device against a user.
        /// </summary>
        /// <param name="notice">Id and payload.</param>
        /// <returns></returns>
        Task<string> SendNotification(SendNotificationDto notice);

        /// <summary>
        /// Initial server setup - get Vapid Keys for public / private.
        /// </summary>
        /// <returns></returns>
        VapidKeyDto GetInitialKeys();
    }
}

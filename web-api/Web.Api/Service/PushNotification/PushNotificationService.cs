using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Web.Api.Configuration;
using Web.Api.Data;
using Web.Api.Data.Models;
using Web.Api.Dto.PushNotification;
using WebPush;

namespace Web.Api.Service.PushNotification
{
    public class PushNotificationService : IPushNotificationService
    {
        private readonly ApplicationDbContext _db;
        private readonly IAppConfiguration Config;

        /// <summary>
        /// Initializes a new instance of the <see cref="PushNotificationService"/> class.
        /// </summary>
        /// <param name="db">Database Context.</param>
        /// <param name="config">App Configuration.</param>
        public PushNotificationService(ApplicationDbContext db, IAppConfiguration config)
        {
            _db = db;
            Config = config;
        }

        /// <inheritdoc />
        public VapidKeyDto GetInitialKeys()
        {
            VapidDetails vapidKeys = VapidHelper.GenerateVapidKeys();
            return new VapidKeyDto
            {
                PublicKey = vapidKeys.PublicKey,
                PrivateKey = vapidKeys.PrivateKey
            };
        }

        /// <inheritdoc />
        public async Task<int> RegisterDevice(RegisterPwaSubscriptionDto pushKey)
        {
            PwaSubscription subScript = await _db.PwaSubscription.Where(x => x.UserId == pushKey.UserId && x.DeviceName == pushKey.DeviceName)
                .SingleOrDefaultAsync();

            // if subscription for user and deviceName already exists - return 0;
            if (subScript != null)
            {
                return 0;
            }

            PwaSubscription newSubScript = new PwaSubscription
            {
                UserId = pushKey.UserId,
                DeviceName = pushKey.DeviceName,
                EndPoint = pushKey.EndPoint,
                KeyP256dh = pushKey.KeyP256dh,
                KeyAuth = pushKey.KeyAuth
            };

            _db.PwaSubscription.Add(newSubScript);
            await _db.SaveChangesAsync();

            return newSubScript.Id;
        }

        /// <inheritdoc />
        public async Task<string> SendNotification(SendNotificationDto notice)
        {
            PwaSubscription subScript = await _db.PwaSubscription.Where(x => x.Id == notice.Id)
                .SingleOrDefaultAsync();

            // if subscription for user and deviceName already exists - return 0;
            if (subScript == null)
            {
                return $"Device with {notice.Id} does not exist in DB.";
            }

            PushSubscription subscription = new PushSubscription(subScript.EndPoint, subScript.KeyP256dh, subScript.KeyAuth);

            string subject = Config.AppSettings.VapidServerKeys.Subject;
            string publicKey = Config.AppSettings.VapidServerKeys.PublicKey;
            string privateKey = Config.AppSettings.VapidServerKeys.PrivateKey;
            var options = new Dictionary<string, object>
            {
                ["vapidDetails"] = new VapidDetails(subject, publicKey, privateKey)
            };

            var webPushClient = new WebPushClient();
            PushNotificationObject noticePayload = new PushNotificationObject
            {
                Notification = new PushNotificationPayload
                {
                    Title = "CX3 POC test",
                    Body = notice.Notification
                }
            };
            try
            {
                await webPushClient.SendNotificationAsync(subscription, JsonSerializer.Serialize(noticePayload), options);
            }
            catch (Exception ex)
            {
                return $"Failure to send Notification: {ex}";
            }


            return "Send Ok";

        }
    }
}

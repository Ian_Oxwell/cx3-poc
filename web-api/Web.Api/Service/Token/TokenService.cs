using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Web.Api.Data;
using Web.Api.Data.Models;
using Web.Api.Configuration;
using System.IdentityModel.Tokens.Jwt;

namespace Web.Api.Service.Token
{
    public class TokenService : ITokenService
    {
        private readonly ApplicationDbContext _db;
        private readonly IAppConfiguration _config;

        /// <summary>
        /// Initializes a new instance of the <see cref="TokenService"/> class.
        /// </summary>
        /// <param name="db">Database Context.</param>
        /// <param name="config">App Configuration.</param>
        public TokenService(ApplicationDbContext db, IAppConfiguration config)
        {
            _db = db;
            _config = config;
        }

        /// <inheritdoc />
        public IEnumerable<Claim> Authenticate(string email)
        {
            var user = GetUser(email);

            // Can't find the user in the database
            if (user == null || user.Id <= 0)
            {
                List<Claim> messageList = new List<Claim>
                {
                    new Claim("user", "register")
                };
                return messageList;
            }
            return GenerateClaims(user);
        }


        /// <inheritdoc />
        public string GenerateRefreshToken(string email, string ipAddress = "127.0.0.1", string refreshToken = "", bool isReissuing = false)
        {

            var refreshLength = GetJwtRefreshLifetime();

            User user = _db.User.SingleOrDefault(x => x.Email.Trim().ToLower() == email.Trim().ToLower());
            if (user == null)
            {
                return null;
            }
            RefreshToken newRefreshToken = GenerateRefreshTokenItem(ipAddress);
            // brand new refresh Token
            if (!isReissuing)
            {
                // Add a new refresh token
                user.RefreshTokens.Add(newRefreshToken);

                // Remove any old tokens
                RemoveOldRefreshTokens(user);
            }
            // update refresh token
            else
            {
                // Do we have a refresh token already?
                var t = user.RefreshTokens.Single(rt => rt.Token == refreshToken);

                // No token found?
                if (t == null)
                {
                    // Log expiry
                    return null;
                }

                // Revoke the old token
                t.Revoked = DateTime.UtcNow;
                t.RevokedByIp = ipAddress;
                t.ReplacedByToken = newRefreshToken.Token;
                t.ModifiedAt = DateTimeOffset.Now;
                user.RefreshTokens.Add(newRefreshToken);
            }
            _db.Update(user);
            // Apply the update/add changes
            _db.SaveChanges();
            return newRefreshToken.Token;
        }

        /// <inheritdoc />
        public bool IsRefreshTokenValid(string email, string refreshToken)
        {
            RefreshToken token;
            var refreshLength = GetJwtRefreshLifetime();


            // Find the token
            User user = _db.User.SingleOrDefault(u => u.Email.Trim().ToLower() == email.Trim().ToLower());
            token = user.RefreshTokens.FindAll(t => t.Token == refreshToken)
                                                    .Find(x => IsActive(x.ModifiedAt.UtcDateTime, x.ModifiedAt.AddMinutes(refreshLength).UtcDateTime));


            // Not found
            if (token == null)
            {
                return false;
            }

            // Valid!
            return true;
        }

        /// <summary>
        /// Get a user by username
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        private User GetUser(string username)
        {
            if (string.IsNullOrWhiteSpace(username))
            {
                return null;
            }

            return _db.User.Where(u => u.Email.ToLower() == username.ToLower().Trim())
                .FirstOrDefault();
        }

        /// <summary>
        /// Generates the claims.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        private IEnumerable<Claim> GenerateClaims(User user)
        {
            var expiresIn =
                new DateTimeOffset(DateTime.Now.AddMinutes(_config.AppSettings.JwtIssuerOptions.JwtLifetime)).ToUnixTimeSeconds().ToString();

            // Generate the base claims
            // TODO: Make the claims JwtRegisteredClaimNames RFC Compliant!!!
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim("email", user.Email),
                new Claim("givenname", user.GivenNames),
                new Claim("familyname", user.FamilyName),
                new Claim(JwtRegisteredClaimNames.Exp, expiresIn)
            };


            // We are authenticated!
            return claims;
        }

        /// <summary>
        /// Gets the JWT refresh lifetime from app settings.
        /// </summary>
        /// <returns></returns>
        private int GetJwtRefreshLifetime()
        {
            return _config.AppSettings.JwtIssuerOptions.JwtLifetime;
        }

        private RefreshToken GenerateRefreshTokenItem(string ipAddress)
        {
            // Generate a Refresh Token - unique GUID
            var token = Guid.NewGuid().ToString().Replace("-", string.Empty, StringComparison.Ordinal);
            return new RefreshToken
            {
                Expires = DateTime.UtcNow.AddMinutes(GetJwtRefreshLifetime()),
                CreatedAt = DateTime.UtcNow,
                Token = token,
                CreatedByIp = ipAddress
            };
        }

        /// <summary>
        /// Removes all expired refresh tokens.
        /// </summary>
        /// <param name="user">User Object.</param>
        private void RemoveOldRefreshTokens(User user)
        {
            user.RefreshTokens.RemoveAll(x =>
                !x.IsActive &&
                x.CreatedAt.AddMinutes(GetJwtRefreshLifetime()) <= DateTime.UtcNow);
        }

        /// <summary>
        /// Determines whether the specified start date is active.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns>
        ///   <c>true</c> if the specified start date is active; otherwise, <c>false</c>.
        /// </returns>
        private static bool IsActive(DateTime startDate, DateTime? endDate)
        {
            var today = DateTime.Now;
            return startDate <= today && (endDate <= today || endDate == null);
        }
    }
}

using System.Collections.Generic;
using System.Security.Claims;

namespace Web.Api.Service.Token
{
    public interface ITokenService
    {
        /// <summary>
        /// Authenticate a user with email - typically would have second match here.
        /// </summary>
        /// <param name="email">the user's email</param>
        /// <returns>A list of claims if successfully authenticated otherwise empty list.</returns>
        IEnumerable<Claim> Authenticate(string email);


        /// <summary>
        /// Generates the refresh token used to reissue expired access tokens.
        /// </summary>
        /// <param name="email">Email of the user.</param>
        /// <param name="refreshToken">The refresh token.</param>
        /// <param name="isReissuing">if set to <c>true</c> [is reissuing].</param>
        /// <param name="ipAddress">IP address of request</param>
        /// <returns></returns>
        string GenerateRefreshToken(string email, string ipAddress = "127.0.0.1", string refreshToken = "", bool isReissuing = false);

        /// <summary>
        /// Determines whether [is refresh token valid] [the specified user name].
        /// </summary>
        /// <param name="email">Email of the user.</param>
        /// <param name="refreshToken">The refresh token.</param>
        /// <returns>
        ///   <c>true</c> if [is refresh token valid] [the specified user name]; otherwise, <c>false</c>.
        /// </returns>
        bool IsRefreshTokenValid(string email, string refreshToken);
    }
}

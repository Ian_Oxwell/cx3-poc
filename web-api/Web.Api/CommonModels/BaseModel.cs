using System.ComponentModel.DataAnnotations;

namespace Web.Api.CommonModels
{
    /// <inheritdoc />
    public class BaseModel : IBaseModel
    {
        /// <inheritdoc />
        public int Id { get; set; }

        /// <inheritdoc />
        [Timestamp]
        public byte[] RowVer { get; set; }
    }
}

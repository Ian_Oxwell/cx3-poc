namespace Web.Api.CommonModels
{
    /// <summary>
    /// Base interface for models in MET.
    /// </summary>
    public interface IBaseModel
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        int Id { get; set; }

        /// <summary>
        /// Gets or sets the row version.
        /// </summary>
        /// <value>
        /// The row version.
        /// </value>
        byte[] RowVer { get; set; }
    }
}

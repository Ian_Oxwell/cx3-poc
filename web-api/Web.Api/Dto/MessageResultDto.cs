namespace Web.Api.Dto
{
    public class MessageResultDto
    {
        public string Message { get; set; }
    }
}

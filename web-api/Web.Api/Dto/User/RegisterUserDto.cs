
using System.Collections.Generic;
using Web.Api.Data.Models;

namespace Web.Api.Dto.User
{
    public class RegisterUserDto
    {
        /// <summary>
        /// Gets or sets the Users Family Name.
        /// </summary>
        public string FamilyName { get; set; }

        /// <summary>
        /// Gets or sets the Users Given Names - notice the plural.
        /// </summary>
        public string GivenNames { get; set; }

        /// <summary>
        /// Gets or sets the Users Mobile number.
        /// </summary>
        public string MobileNumber { get; set; }

        /// <summary>
        /// Gets or sets the Users Email address - unique.
        /// </summary>
        public string Email { get; set; }

    }
}

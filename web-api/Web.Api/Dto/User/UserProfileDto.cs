using System.Collections.Generic;
using Web.Api.Dto.PushNotification;

namespace Web.Api.Dto.User
{
    public class UserProfileDto : BaseModelDto
    {
        /// <summary>
        /// Gets or sets the Users email address.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the Users Family Name.
        /// </summary>
        public string FamilyName { get; set; }

        /// <summary>
        /// Gets or sets the Users Given Names - notice the plural.
        /// </summary>
        public string GivenNames { get; set; }

        /// <summary>
        /// Gets or sets the user's name formatted as "FirstName LastName".
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets the Users Mobile number.
        /// </summary>
        public string MobileNumber { get; set; }

        /// <summary>
        /// Gets or sets the collection of Subscription Keys.
        /// </summary>
        public IEnumerable<PwaSubscriptionKeyDto> SubscriptionKeys { get; set; }
    }
}

namespace Web.Api.Dto.PushNotification
{
    public class SendNotificationDto
    {
        /// <summary>
        /// Subscription Id - sent from Front End.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Notice Payload.
        /// </summary>
        public string Notification { get; set; }
    }
}

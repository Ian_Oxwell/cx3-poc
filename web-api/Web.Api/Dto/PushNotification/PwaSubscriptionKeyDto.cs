namespace Web.Api.Dto.PushNotification
{
    public class PwaSubscriptionKeyDto : BaseModelDto
    {
        /// <summary>
        /// Gets or sets the friendly device name (only needed for testing).
        /// </summary>
        public string DeviceName { get; set; }
    }
}

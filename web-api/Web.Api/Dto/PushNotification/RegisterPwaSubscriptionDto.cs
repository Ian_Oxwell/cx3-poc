namespace Web.Api.Dto.PushNotification
{
    public class RegisterPwaSubscriptionDto
    {
        /// <summary>
        /// Gets or sets the friendly device name (only needed for testing).
        /// </summary>
        public string DeviceName { get; set; }

        /// <summary>
        /// Gets or sets the Google Api endpoint to call.
        /// </summary>
        public string EndPoint { get; set; }

        /// <summary>
        /// Gets or sets the p256dh key.
        /// </summary>
        public string KeyP256dh { get; set; }

        /// <summary>
        /// Gets or sets the Authorisation Key.
        /// </summary>
        public string KeyAuth { get; set; }

        /// <summary>
        /// Gets or sets the related UserId.
        /// </summary>
        public int UserId { get; set; }
    }
}

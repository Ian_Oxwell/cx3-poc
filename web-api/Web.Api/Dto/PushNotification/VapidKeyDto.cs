using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Api.Dto.PushNotification
{
    public class VapidKeyDto
    {

        /// <summary>
        /// Gets or sets the Vapid Public Key.
        /// </summary>
        public string PublicKey { get; set; }

        /// <summary>
        /// Gets or sets the Vapid Private Key.
        /// </summary>
        public string PrivateKey { get; set; }
    }
}

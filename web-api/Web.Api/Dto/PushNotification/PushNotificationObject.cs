namespace Web.Api.Dto.PushNotification
{
    public class PushNotificationObject
    {
        /// <summary>
        /// Get or Set the Notification Payload object.
        /// </summary>
        public PushNotificationPayload Notification { get; set; }
    }
}

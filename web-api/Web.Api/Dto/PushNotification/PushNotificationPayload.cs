namespace Web.Api.Dto.PushNotification
{
    /// <summary>
    /// Contents in the Notification
    /// See here for complete list that can be included - https://notifications.spec.whatwg.org/#api
    /// Also see here https://blog.angular-university.io/angular-push-notifications/ - search for 'payload'
    /// </summary>
    public class PushNotificationPayload
    {
        /// <summary>
        /// Get or Set the Title for the message.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// The Message to display.
        /// </summary>
        public string Body { get; set; }
    }
}

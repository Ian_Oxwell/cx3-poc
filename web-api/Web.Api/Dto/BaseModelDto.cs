using System;

namespace Web.Api.Dto
{
    public partial class BaseModelDto
    {
        /// <summary>
        /// Gets or sets the primary key.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the Date time object created.
        /// </summary>
        public DateTimeOffset CreatedAt { get; set; }

        /// <summary>
        /// Gets or sets the row version for the entity.
        /// </summary>
        public byte[] RowVer { get; set; }
    }
}

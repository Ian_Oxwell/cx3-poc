# Cx3 Proof Of Concept API

This project is a proof of concept for PWA service worker.<br/>
[can I Use](https://caniuse.com/serviceworkers)<br/>
[Service Workers](https://web.dev/progressive-web-apps/)<br/>
[Web push CSharp](https://github.com/web-push-libs/web-push-csharp)

## Database

Create local database: RollerPocCx<br/>
Assign RollerDevUser as admin<br/>

In Package Manager Console run the following command:<br/>
`Update-Database`

## Build
Run IIS Express for local server on port 44393<br/>
[Swagger Doc](https://localhost:44393/swagger/index.html)



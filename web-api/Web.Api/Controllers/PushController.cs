using Microsoft.AspNetCore.Mvc;
using Web.Api.Dto;
using Web.Api.Dto.PushNotification;
using Web.Api.Service.PushNotification;

namespace Web.Api.Controllers
{
    [Route("api/push")]
    [ApiController]
    public class PushController : ControllerBase
    {
        private readonly IPushNotificationService PushNotification;

        public PushController(IPushNotificationService pushNotificationService)
        {
            PushNotification = pushNotificationService;
        }

        /// <summary>
        /// Creates a new device subscription and google endpoint.
        /// </summary>
        /// <param name="keyModel">Data obtained from the front end for the respected device keys and endpoints.</param>
        /// <returns>int of the newly created subscription.</returns>
        [HttpPost("subscription")]
        [ProducesResponseType(200, Type = typeof(MessageResultDto))]
        public IActionResult AddSubscription(RegisterPwaSubscriptionDto keyModel)
        {
            if (keyModel == null)
            {
                return BadRequest(new { message = "Invalid Model, try again." });
            }
            int pushSubscription = PushNotification.RegisterDevice(keyModel).Result;

            if (pushSubscription == 0)
            {
                return Conflict(new { message = "Error, item found with same device name for user." });
            }
            return Ok(pushSubscription);
        }

        /// <summary>
        /// TEMP - gets the servers keys to use in the front end.
        /// </summary>
        /// <returns>public and private keys.</returns>
        [HttpGet]
        [ProducesResponseType(200, Type = typeof(VapidKeyDto))]
        public IActionResult GetServerKeys()
        {
            VapidKeyDto keys = PushNotification.GetInitialKeys();
            return Ok(keys);
        }

        /// <summary>
        /// Post Notice to Device Id.
        /// </summary>
        /// <param name="notice">Subscription Id and the notice payload.</param>
        /// <returns>Message result.</returns>
        [HttpPost("notice")]
        [ProducesResponseType(200, Type = typeof(MessageResultDto))]
        public IActionResult SendNotice(SendNotificationDto notice)
        {
            if (notice == null)
            {
                return BadRequest(new { message = "Invalid Model, try again." });
            }

            string pushResult = PushNotification.SendNotification(notice).Result;

            return Ok(new { message = pushResult });
        }
    }
}

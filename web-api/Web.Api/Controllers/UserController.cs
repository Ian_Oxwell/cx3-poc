using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Api.Dto;
using Web.Api.Dto.PushNotification;
using Web.Api.Dto.User;
using Web.Api.Service.PushNotification;
using Web.Api.Service.UserServices;

namespace Web.Api.Controllers
{
    /// <summary>
    /// Contains API endpoints to fetch, update and add user accounts.
    /// </summary>
    [Route("api/user")]
    [ApiController]
    [AllowAnonymous]
    [Produces("application/json")]
    [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(string), StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(string), StatusCodes.Status403Forbidden)]
    [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)]
    public class UserController : Controller
    {
        private readonly IUserService UserService;


        public UserController(IUserService userService)
        {
            UserService = userService;
        }

        /// <summary>
        /// Checks if the email address is already registered or if it is available.
        /// </summary>
        /// <param name="email">string of the email address.</param>
        /// <returns>true if email is available, false if already registered user with that email address.</returns>
        [HttpGet("available")]
        [ProducesResponseType(200, Type = typeof(bool))]
        public IActionResult CheckEmailAddressUsed(string email)
        {
            bool userNameExists = UserService.UserNameAvailable(email);
            return Ok(userNameExists);
        }

        /// <summary>
        /// Gets user by Id.
        /// </summary>
        /// <param name="id">Users Id in the DB.</param>
        /// <returns>User Profile.</returns>
        [HttpGet("user")]
        [ProducesResponseType(200, Type = typeof(UserProfileDto))]
        public IActionResult GetUserProfile(int id)
        {
            if (id == 0)
            {
                return BadRequest(new { message = "Invalid user ID" });
            }
            UserProfileDto account = UserService.GetSingleUser(id).Result;
            if (account == null)
            {
                return NotFound(new { message = "Account not found or permitted" });
            }
            return Ok(account);
        }

        /// <summary>
        /// Creates a new User.
        /// </summary>
        /// <param name="model">shortened User Profile model.</param>
        /// <returns>Message containing the new user id.</returns>
        [HttpPost("user")]
        [ProducesResponseType(200, Type = typeof(MessageResultDto))]

        public IActionResult Register(RegisterUserDto model)
        {
            if (!UserService.UserNameAvailable(model.Email))
            {
                return Ok(new { message = "Email address is already registered" });
            }
            int user = UserService.RegisterUser(model).Result;

            return Ok(new { message = user });
        }

    }
}

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Pcb.Api.Auth;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using Web.Api.Configuration;
using Web.Api.Service.Token;

namespace Web.Api.Controllers
{
    [Route("api/token")]
    [ApiController]
    [AllowAnonymous]
    [Produces("application/json")]
    [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(string), StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(string), StatusCodes.Status403Forbidden)]
    [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)]
    public class TokenController : ControllerBase
    {

        /// <summary>
        /// The configuration instance from DI.
        /// </summary>
        private IAppConfiguration Config { get; }

        /// <summary>
        /// The Jwt factory instance from DI.
        /// </summary>
        private IJwtFactory JwtFactory { get; }

        private ITokenService TokenService { get; }

        public TokenController(IAppConfiguration config, IJwtFactory jwtFactory, ITokenService tokenService)
        {
            Config = config;
            JwtFactory = jwtFactory;
            TokenService = tokenService;
        }

        [Route("create")]
        [HttpPost]
        public IActionResult Create(string email)
        {
            // Validate input
            if (email!.Length == 0)
            {
                return BadRequest();
            }


            // Try and generate a token
            return TryPassword(email);
        }

        /// <summary>
        /// Attempts to reissue a access token using a refresh token (GUID).
        /// </summary>
        /// <param name="email">Users email address.</param>
        /// <param name="refreshToken">Current Refresh Token.</param>
        /// <returns></returns>
        [Route("refresh")]
        [HttpPost]
        public IActionResult Refresh(string email, string refreshToken)
        {
            // Validate input
            if (email!.Length == 0 || refreshToken!.Length == 0)
            {
                return BadRequest();
            }

            // Try to reissue the token
            return TryRefreshToken(refreshToken, email);
        }

        /// <summary>
        /// Issues an access token and a refresh token.
        /// </summary>
        /// <param name="email">Name of the user.</param>
        /// <returns></returns>
        private IActionResult TryPassword(string email)
        {
            // Authenticate with credentials
            var claims = TokenService.Authenticate(email).ToList();

            // Are there any claims?
            if (!claims.Any())
            {
                return BadRequest(new { message = "Password is invalid" });
            }
            if (claims.Count == 1 && claims[0].Type == "user")
            {
                return Ok(new { message = "Register new account" });
            }

            if (claims.Count == 1 && claims[0].Type == "verify")
            {
                return Ok(new { message = "Account not verified, please check your emails." });
            }

            if (claims.Count == 1 && claims[0].Type == "authentication")
            {
                return Ok(new { message = $"Lockout: {claims[0].Value}" });
            }

            // Failed Auth - Deactivated user
            if (claims.FirstOrDefault(c => c.Type == JwtRegisteredClaimNames.Sub && c.Value == "0") != null)
            {
                return Forbid();
            }

            // Obtain a refresh token
            var refreshToken = TokenService.GenerateRefreshToken(email, IpAddress());

            // Did we even get a token?
            if (string.IsNullOrWhiteSpace(refreshToken))
            {
                return BadRequest(new { message = "No refresh token was generated from the security service." });
            }

            var jwtToken = GetJwt(refreshToken, claims);


            // Return our tokens all polished up and ready to go!
            return jwtToken;
        }

        /// <summary>
        /// Tries the refresh token.
        /// On Success - Returns a new access token and a new refresh token
        /// </summary>
        /// <param name="refreshToken">The refresh token.</param>
        /// <param name="email">Name of the user.</param>
        /// <returns></returns>
        private IActionResult TryRefreshToken(string refreshToken, string email)
        {
            // Refresh Token Valid?
            if (!TokenService.IsRefreshTokenValid(email, refreshToken))
            {
                return NotFound(new { message = "Refreshing Token: Cannot reissue a refresh token because the current refresh token has expired!" });
            }

            // Get new claims
            var claims = TokenService.Authenticate(email).ToList();

            // Are there any claims?
            if (!claims.Any())
            {
                return Unauthorized(new { message = $"Refreshing Token: Cannot create claims for user '{email}'." });
            }

            // Expire the old refresh token
            // Create a new refresh token
            var newRefreshToken = TokenService.GenerateRefreshToken(email, IpAddress(), refreshToken, true);

            // Did we even get a token?
            if (string.IsNullOrWhiteSpace(newRefreshToken))
            {
                return NotFound(new { message = "Refreshing Token: No refresh token was generated from the security service." });
            }

            // Return our shiny tokens!
            return GetJwt(newRefreshToken, claims);
        }

        /// <summary>
        /// Gets the JWT and constructs the result.
        /// </summary>
        /// <param name="refreshToken">The refresh token.</param>
        /// <param name="claims">The claims.</param>
        /// <returns></returns>
        private JsonResult GetJwt(string refreshToken, IEnumerable<Claim> claims)
        {
            var token = JwtFactory.GenerateToken(claims);

            var response = new
            {
                token,
                expiresIn = (int)TimeSpan.FromMinutes(Config.AppSettings.JwtIssuerOptions.JwtLifetime).TotalSeconds,
                refreshToken
            };

            return new JsonResult(response);
        }

        /// <summary>
        /// Gets the users IP Address from the request headers.
        /// </summary>
        /// <returns>IP address string</returns>
        private string IpAddress()
        {
            if (Request.Headers.ContainsKey("X-Forwarded-For"))
                return Request.Headers["X-Forwarded-For"];
            else
                return HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
        }
    }
}
